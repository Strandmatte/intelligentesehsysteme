Run ImageToolBox
========================


1. Download and extract the ZIP-File "ImageToolBox.zip" or "ImageToolBox.exe", respectively.

   The following directory tree is created:


   ImageToolBox
       |
       |
       +-- filter    (directory for your implemented filters. *.java and *.class)
       |
       +-- image     (directory with sample images)
       |
       +-- src       (directory with source code of the ImageToolBox)
       |
       +-- temp      (directory with temporary files of the ImageToolBox)


2. Change to the directory "ImageToolBox" or where you extracted the files, respectively.

3. Run the ImageToolBox from command line by "java -jar ImageToolBox.jar".




Tips
========


1. If you load many images, then sometimes the error "Out of memory error" occures.
   This can be fixed by enlarging the heap size.

   Example:

   "java -Xmx200m -jar ImageToolBox"

   This sets the heap size to 200 MB.


2. Due to the reflection mechanism of Java, you can only load filters from directories
   contained in the classpath. To include additional directories, you can add "-cp" 
   and the corresponding directory in the command line.
   
   Example:
   
   "java -cp myfilters -jar ImageToolBox"

   This will add the directory myfilters in the current working directory 
   to the classpath.
