Starten von ImageToolBox
========================


1. ZIP-File "ImageToolBox.zip" bzw "ImageToolBox.exe" downloaden und extrahieren.

   Es entsteht folgender Verzeichnisbaum:


   ImageToolBox
       |
       |
       +-- filter    (Verzeichnis f�r selbstprogrammierte Filter. *.java und *.class)
       |
       +-- image     (Verzeichnis f�r Bilder)
       |
       +-- src       (Verzeichnis mit den Quelldateien der ImageToolBox)
       |
       +-- temp      (Verzeichnis f�r die tempor�ren Dateien)


2. Ins Verzeichnis "ImageToolBox" wechseln.

3. Starten mit "java -jar ImageToolBox.jar"




Hinweise
========


1. Wenn viele Bilder geladen worden sind kann evtl. ein "Out of memory error" auftreten.
   Um dies zu Verhindern sollte beim starten die maximale Gr��e des Heaps erh�ht werden.

   Beispiel:

   "java -Xmx200m -jar ImageToolBox"

   Hierdurch wird die maximale Heapgr��e auf 200 MB gesetzt.


2. Es k�nnen nur Filter aus Verzeichnissen geladen werden, die mittels "-cp"
   angegeben wurden.
   
   Beispiel:
   
   "java -cp myfilters -jar
