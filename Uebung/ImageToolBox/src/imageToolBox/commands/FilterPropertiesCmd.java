package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class FilterPropertiesCmd extends Command
{
  protected ImageToolBox itb;

  public FilterPropertiesCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }

  public void execute()
  {
    itb.filterProperties();
  }
}
