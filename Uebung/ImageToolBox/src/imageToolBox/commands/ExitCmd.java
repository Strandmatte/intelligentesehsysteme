package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class ExitCmd extends Command
{
  protected ImageToolBox itb;

  public ExitCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }

  public void execute()
  {
    itb.Exit();
  }
}
