package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class DeleteFilterCmd extends Command
{
  protected ImageToolBox itb;

  public DeleteFilterCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }

  public void execute()
  {
    itb.deleteFilter();
  }
}
