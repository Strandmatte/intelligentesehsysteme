package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class FitToScreenCmd extends Command
{
  protected ImageToolBox itb;

  public FitToScreenCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }

  public void execute()
  {
    itb.fitToScreen();
  }
}
