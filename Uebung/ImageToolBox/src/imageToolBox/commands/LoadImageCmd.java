package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class LoadImageCmd extends Command
{
  protected ImageToolBox itb;
  
  public LoadImageCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }
  
  public void execute()
  {
    itb.loadImage();
  }
}
