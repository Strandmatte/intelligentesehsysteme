package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class PrevImageCmd extends Command
{
  protected ImageToolBox itb;

  public PrevImageCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }

  public void execute()
  {
    itb.prevImage();
  }

}
