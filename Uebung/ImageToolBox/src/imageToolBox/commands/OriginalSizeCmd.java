package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class OriginalSizeCmd extends Command
{
  protected ImageToolBox itb;

  public OriginalSizeCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }

  public void execute()
  {
    itb.originalSize();
  }
}
