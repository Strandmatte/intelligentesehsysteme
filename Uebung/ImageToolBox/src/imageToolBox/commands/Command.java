package imageToolBox.commands;

public abstract class Command
{
  public abstract void execute();
}
