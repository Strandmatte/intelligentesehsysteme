package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class CropImageCmd extends Command
{
  protected ImageToolBox itb;
  
  public CropImageCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }
  @Override
  public void execute()
  {
    itb.cropImage();    
  }
 
}
