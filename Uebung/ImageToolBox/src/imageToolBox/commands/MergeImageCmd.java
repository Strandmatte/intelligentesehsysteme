package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class MergeImageCmd extends Command
{
  protected ImageToolBox itb;

  public MergeImageCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }

  public void execute()
  {
    itb.openMerge();
  }
}
