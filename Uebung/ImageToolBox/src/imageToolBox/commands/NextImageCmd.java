package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class NextImageCmd extends Command
{
  protected ImageToolBox itb;

  public NextImageCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }

  public void execute()
  {
    itb.nextImage();
  }
}
