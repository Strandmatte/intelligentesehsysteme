package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class SpectrumCmd extends Command
{
 protected ImageToolBox itb;
  
  public SpectrumCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }
  
  public void execute()
  {
    itb.openSpectrum();
  }
}
