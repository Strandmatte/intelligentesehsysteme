package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class LoadFilterCmd extends Command
{
  protected ImageToolBox itb;

  public LoadFilterCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }

  public void execute()
  {
    itb.loadFilter();
  }
}
