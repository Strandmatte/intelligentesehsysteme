package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class DeleteImageCmd extends Command
{
  protected ImageToolBox itb;
  
  public DeleteImageCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }
  
  public void execute()
  {
    itb.deleteImage();
  }
}
