package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class SaveImageCmd extends Command
{
 protected ImageToolBox itb;
  
  public SaveImageCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }
  
  public void execute()
  {
    itb.saveImage();
  }
}
