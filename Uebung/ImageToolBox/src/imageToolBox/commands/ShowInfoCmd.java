package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class ShowInfoCmd extends Command
{
  protected ImageToolBox itb;

  public ShowInfoCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }

  public void execute()
  {
    itb.openInfo();
  }
}
