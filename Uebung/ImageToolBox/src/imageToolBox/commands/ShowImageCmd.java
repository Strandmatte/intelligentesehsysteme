package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class ShowImageCmd extends Command
{
 protected ImageToolBox itb;
  
  public ShowImageCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }
  
  public void execute()
  {
    itb.showImage();
  }
}
