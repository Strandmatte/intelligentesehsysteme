package imageToolBox.commands;

import imageToolBox.ImageToolBox;

public class ApplyFilterCmd extends Command
{
  protected ImageToolBox itb;

  public ApplyFilterCmd(ImageToolBox itb)
  {
    this.itb = itb;
  }

  public void execute()
  {
    itb.startThread(ImageToolBox.methodApplyFilter);
  }
}
