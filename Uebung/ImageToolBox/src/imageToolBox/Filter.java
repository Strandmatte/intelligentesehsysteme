package imageToolBox;
import java.awt.CheckboxMenuItem;

public class Filter
{
  Object instance;
  String filename;
  String name;
  CheckboxMenuItem mi;

  public Filter(Object i, String f, String n)
  {
    instance = i;
    filename = f;
    name = n;
    mi = new CheckboxMenuItem(name, false);
  }
}