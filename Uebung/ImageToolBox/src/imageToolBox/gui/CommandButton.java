package imageToolBox.gui;

import imageToolBox.commands.Command;
import java.awt.Button;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CommandButton extends Button implements ActionListener
{
  protected Command command;
  
  public CommandButton(String caption, Command cmd)
  {
    super(caption);    
    command = cmd;
    
    addActionListener(this);
  }
  
  public void actionPerformed(ActionEvent event)
  {
    command.execute(); 
  }  
}
