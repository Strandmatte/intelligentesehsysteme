package imageToolBox.gui;

import imageToolBox.commands.Command;

import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class CommandMenuItem extends MenuItem implements ActionListener
{
  protected Command command;
  
  public CommandMenuItem(String caption, Command cmd)
  {
    super(caption);    
    command = cmd;
    
    addActionListener(this);
  }
  
  public void actionPerformed(ActionEvent event)
  {
    command.execute(); 
  }  

}
