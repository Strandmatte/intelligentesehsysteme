package imageToolBox.gui;

import imageToolBox.commands.Command;

import java.awt.CheckboxMenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class CommandCheckboxMenuItem extends CheckboxMenuItem implements ItemListener
{
  protected Command command;
  
  public CommandCheckboxMenuItem(String caption, Command cmd)
  {
    super(caption);    
    command = cmd;
    
    addItemListener(this);
  }

  public void itemStateChanged(ItemEvent arg0)
  {
//    if(!getState())
//      setState(true);
//    else
//      setState(false);
    
    command.execute(); 
  }  
}
