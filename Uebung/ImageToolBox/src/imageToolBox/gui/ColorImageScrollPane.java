package imageToolBox.gui;

import imageToolBox.ColorImage;

import java.awt.BasicStroke;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Panel;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.ScrollPane;
import java.awt.Scrollbar;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.awt.image.RasterFormatException;
import java.awt.image.RescaleOp;

/**
 * scrollbares, zoombares und beschneidbares Anzeigefenster
 * 
 * @author jens behley
 *
 */
public class ColorImageScrollPane extends Panel implements ComponentListener,
    AdjustmentListener, MouseListener, MouseMotionListener
{
  protected static BasicStroke DASHED_STROKE;
  protected static final BasicStroke DEFAULT_STROKE = new BasicStroke();

  protected ColorImage child;
  protected ColorImageScrollPaneProperties properties;

  protected BufferedImage offscreen;
  protected Graphics2D offscreenGraphics;

  protected Scrollbar hScrollbar;
  protected Scrollbar vScrollbar;

  protected boolean clearPanel = false;

  protected boolean dragging = false;
  protected Point startPoint;
  protected Point endPoint;
  protected int childX = 0;
  protected int childY = 0;
  protected int clipX = 0;
  protected int clipY = 0;
  protected int clipWidth = 0;
  protected int clipHeight = 0;

  public static final int MODE_ZOOM = 0;
  public static final int MODE_CROP = 1;
  protected int mode = MODE_ZOOM;
  // protected Rectangle cropSelection;

  protected int draggedRegion = -1;
  protected static final int NOTHING = -1;
  protected static final int NORTH = 0;
  protected static final int NORTHEAST = 1;
  protected static final int EAST = 2;
  protected static final int SOUTHEAST = 3;
  protected static final int SOUTH = 4;
  protected static final int SOUTHWEST = 5;
  protected static final int WEST = 6;
  protected static final int NORTHWEST = 7;
  protected static final int INREGION = 8;

  protected Cursor currentCursor = Cursor.getDefaultCursor();

  public ColorImageScrollPane(ColorImage child)
  {
    super();
    this.child = child;
    addComponentListener(this);
    addMouseMotionListener(this);
    addMouseListener(this);

    properties = new ColorImageScrollPaneProperties();

    hScrollbar = new Scrollbar(Scrollbar.HORIZONTAL);
    vScrollbar = new Scrollbar(Scrollbar.VERTICAL);
    hScrollbar.addAdjustmentListener(this);
    vScrollbar.addAdjustmentListener(this);
    hScrollbar.setVisible(false);
    vScrollbar.setVisible(false);
    add(hScrollbar);
    add(vScrollbar);

    offscreen = child.bi;
    offscreenGraphics = (Graphics2D) offscreen.getGraphics();
    setBackground(new Color(236, 233, 216));

    // 
    float[] dash = { 1.0f, 0.5f };
    DASHED_STROKE = new BasicStroke(1.0f, BasicStroke.CAP_BUTT,
        BasicStroke.JOIN_BEVEL, 10.0f, dash, 1.0f);
    // cropSelection = null;
  }

  public ColorImageScrollPane(ColorImage child,
      ColorImageScrollPaneProperties prop)
  {

  }

  /**
   * Properties des Scrollpanes setzen
   * 
   * @param prop neue properties
   */
  public void setProperties(ColorImageScrollPaneProperties prop)
  {
    if (prop == null)
      properties = new ColorImageScrollPaneProperties();
    else
      properties = prop;

    // updateScrollbars();
    updateOffscreen();

    startPoint = null;
    endPoint = null;
  }

  public ColorImageScrollPaneProperties getProperties()
  {
    return properties;
  }

  /**
   * Updating the Offscreen Image
   * 
   */
  protected void updateOffscreen()
  {
    // clear the offscreen
    offscreenGraphics.setColor(getBackground());
    offscreenGraphics.fillRect(0, 0, getWidth(), getHeight());

    if (child != null)
    {
      BufferedImage childBI = child.bi;

      childX = 0;
      childY = 0;
      clipX = 0;
      clipY = 0;
      clipWidth = child.sizeX;
      clipHeight = child.sizeY;

      float scaleFactor = 1.0f;

      if (!properties.fitToView)
      {
        clipX = properties.x;
        clipY = properties.y;
      }
      else
      {
        // skalierung berechnen
        clipX = 0;
        clipY = 0;
        properties.x = 0;
        properties.y = 0;
        
        double scaleFactorY = (float) getHeight() / (float) child.sizeY;  
        double scaleFactorX = (float) getWidth() / (float) child.sizeX;

        properties.scaleFactor = (float)Math.min(scaleFactorX, scaleFactorY);
      }

      if (child.sizeX < getWidth() / properties.scaleFactor)
      {
        clipX = 0;
        childX = (int) ((getWidth() - clipWidth * properties.scaleFactor) / 2.0);
      }
      else
        clipWidth = (int) Math.floor(getWidth() / properties.scaleFactor);

      if (child.sizeY < getHeight() / properties.scaleFactor)
      {
        clipY = 0;
        childY = (int) ((getHeight() - clipHeight * properties.scaleFactor) / 2.0);
      }
      else
        clipHeight = (int) Math.floor(getHeight() / properties.scaleFactor);

      AffineTransform scaleTransform = AffineTransform.getScaleInstance(
          properties.scaleFactor, properties.scaleFactor);
      AffineTransformOp rescaling = new AffineTransformOp(scaleTransform,
          AffineTransformOp.TYPE_BILINEAR);
      BufferedImage clip = null;
      try
      {
        if (clipWidth + clipX > child.sizeX)
        {
          clipX = child.sizeX - clipWidth;
          properties.x = clipX;
        }

        if (clipHeight + clipY > child.sizeY)
        {
          clipY = child.sizeY - clipHeight;
          properties.y = clipY;
        }
        clip = childBI.getSubimage(clipX, clipY, clipWidth, clipHeight);
        offscreenGraphics.drawImage(clip, rescaling, childX, childY);
      }
      catch (RasterFormatException e)
      {
        System.err.println("x: " + clipX + " y: " + clipY + " width: "
            + clipWidth + " height: " + clipHeight);
        System.err.println("oops?! this should never happend!");
      }

      if (mode == MODE_CROP)
        drawCropSelection(startPoint, endPoint);
    }

    repaint();
  }

  protected void resizeOffscreen()
  {
    offscreen = new BufferedImage(getWidth(), getHeight(),
        BufferedImage.TYPE_4BYTE_ABGR);
    if (offscreenGraphics != null)
      offscreenGraphics.dispose();
    offscreenGraphics = (Graphics2D) offscreen.getGraphics();
  }

  public void resetToOriginalSize()
  {
    properties.scaleFactor = 1.0f;
    properties.x = 0;
    properties.y = 0;
    properties.fitToView = false;

    updateOffscreen();
    // updateScrollbars();
    // repaint();
  }

  public void fitToScreen()
  {
    properties.fitToView = true;

    updateOffscreen();
  }

  /*
   * Berechnung der Scrollbardimensionen und Sichtbarkeit
   */
  protected void updateScrollbars()
  {
    if (getWidth() > child.getWidth() * properties.scaleFactor
        || properties.fitToView)
      hScrollbar.setVisible(false);
    else
      hScrollbar.setVisible(true);

    if (getHeight() > child.getHeight() * properties.scaleFactor
        || properties.fitToView)
      vScrollbar.setVisible(false);
    else
      vScrollbar.setVisible(true);

    // reposition the scrollbars
    if (hScrollbar.isVisible() && vScrollbar.isVisible())
    {
      int hHeight = 16;
      int vWidth = 16;
      hScrollbar.setLocation(0, getHeight() - hHeight);
      hScrollbar.setSize(getWidth() - vWidth, hHeight);

      vScrollbar.setLocation(getWidth() - vWidth, 0);
      vScrollbar.setSize(vWidth, getHeight() - hHeight);

      hScrollbar.setMaximum((int) (child.getWidth() * properties.scaleFactor)
          - vWidth);
      hScrollbar.setVisibleAmount(getWidth() - vWidth);

      vScrollbar.setMaximum((int) (child.getHeight() * properties.scaleFactor)
          - hHeight);
      vScrollbar.setVisibleAmount(getHeight() - hHeight);
    }
    else if (hScrollbar.isVisible())
    {

      int hHeight = 16;
      hScrollbar.setLocation(0, getHeight() - hHeight);
      hScrollbar.setSize(getWidth(), hHeight);
      hScrollbar.setMaximum((int) (child.getWidth() * properties.scaleFactor));
      hScrollbar.setVisibleAmount(getWidth());
    }
    else if (vScrollbar.isVisible())
    {
      int vWidth = 16;
      vScrollbar.setLocation(getWidth() - vWidth, 0);
      vScrollbar.setSize(vWidth, getHeight());
      vScrollbar.setMaximum((int) (child.getHeight() * properties.scaleFactor));
      vScrollbar.setVisibleAmount(getHeight());
    }

    hScrollbar
        .setValue((int) Math.floor(properties.x * properties.scaleFactor));
    vScrollbar
        .setValue((int) Math.floor(properties.y * properties.scaleFactor));
    hScrollbar.repaint();
    vScrollbar.repaint();
  }

  public void paint(Graphics g)
  {
    // offscreen image zeichnen
    if (offscreen != null)
    {
      updateScrollbars();

      g.drawImage(offscreen, 0, 0, null);

      // graues Quadrat in unterer rechter Ecke
      if (hScrollbar.isVisible() && vScrollbar.isVisible())
      {
        g.setColor(hScrollbar.getBackground());
        g.fillRect(getWidth() - vScrollbar.getWidth(), getHeight()
            - hScrollbar.getHeight(), getWidth(), getHeight());
      }
    }
  }

  public void update(Graphics g)
  {
    paint(g);
  }

  public void componentHidden(ComponentEvent arg0)
  {

  }

  public void componentMoved(ComponentEvent arg0)
  {

  }

  public void componentResized(ComponentEvent arg0)
  {
    if (getWidth() == 0 || getHeight() == 0)
      return;
    resizeOffscreen();
    updateOffscreen();
    // updateScrollbars();
  }

  public void componentShown(ComponentEvent arg0)
  {
    // updateScrollbars();
  }

  public void adjustmentValueChanged(AdjustmentEvent event)
  {
    if (event.getSource() == hScrollbar)
    {
      properties.x = (int) Math
          .floor(event.getValue() / properties.scaleFactor);
    }
    else if (event.getSource() == vScrollbar)
    {
      properties.y = (int) Math
          .floor(event.getValue() / properties.scaleFactor);
    }
    updateOffscreen();
  }

  /**
   * Auswahlrechteck einzeichnen
   * 
   * @param s startpunkt
   * @param e endpunkt
   */
  protected void drawSelection(Point s, Point e)
  {
    if(s == null || e == null)
      return;
    
    int selX = (startPoint.x < endPoint.x) ? startPoint.x : endPoint.x;
    int selY = (startPoint.y < endPoint.y) ? startPoint.y : endPoint.y;
    int selWidth = (int) Math.abs(endPoint.x - startPoint.x);
    int selHeight = (int) Math.abs(endPoint.y - startPoint.y);

    offscreenGraphics.setStroke(DASHED_STROKE);
    offscreenGraphics.setXORMode(Color.black);
    offscreenGraphics.setColor(Color.WHITE);
    offscreenGraphics.drawRect(selX, selY, selWidth, selHeight);
    offscreenGraphics.setPaintMode();
    offscreenGraphics.setStroke(DEFAULT_STROKE);
  }

  protected void drawCropSelection(Point startPoint, Point endPoint)
  {
    if (startPoint == null || endPoint == null)
      return;
    offscreenGraphics.setStroke(DEFAULT_STROKE);
    offscreenGraphics.setXORMode(Color.black);
    offscreenGraphics.setColor(Color.WHITE);

    int ulx = (int) Math.rint((startPoint.x * properties.scaleFactor + childX));
    int uly = (int) Math.rint((startPoint.y * properties.scaleFactor + childY));
    int lrx = (int) Math.rint((endPoint.x * properties.scaleFactor + childX));
    int lry = (int) Math.rint((endPoint.y * properties.scaleFactor + childY));

    offscreenGraphics.drawLine(ulx, uly, ulx, lry);
    offscreenGraphics.drawLine(ulx + 1, uly, lrx, uly);
    offscreenGraphics.drawLine(ulx + 1, lry, lrx, lry);
    offscreenGraphics.drawLine(lrx, uly + 1, lrx, lry - 1);
    offscreenGraphics.setPaintMode();
  }
  
  public Point getMousePosInsideImage(final Point p) {
	  Point query = p;
	  if (!isMousePosInsideImageX(p)) {
		  if (p.x < childX) query.x = childX+clipX;
		  if (p.x >= childX+child.sizeX) query.x = (int) Math.floor(childX+clipX+child.sizeX*properties.scaleFactor);
	  }
	  if (!isMousePosInsideImageY(p)) {
		  if (p.y < childY) query.y = childY+clipY;
		  if (p.y >= childY+child.sizeY) query.y = (int) Math.floor(childY+clipY+child.sizeY*properties.scaleFactor);
	  }
	  return new Point((int) ((query.x - childX) / properties.scaleFactor + clipX),
	        (int) ((query.y - childY) / properties.scaleFactor + clipY));
  }

  protected int getCropRegion(Point p)
  {
    Point sp = new Point((int) ((p.x - childX) / properties.scaleFactor),
        (int) ((p.y - childY) / properties.scaleFactor));

    if (Math.abs(startPoint.x - sp.x) < 5.0
        && Math.abs(startPoint.y - sp.y) < 5.0)
      return NORTHWEST;

    if (Math.abs(endPoint.x - sp.x) < 5.0
        && Math.abs(startPoint.y - sp.y) < 5.0)
      return NORTHEAST;

    if (Math.abs(endPoint.x - sp.x) < 5.0 && Math.abs(endPoint.y - sp.y) < 5.0)
      return SOUTHEAST;

    if (Math.abs(startPoint.x - sp.x) < 5.0
        && Math.abs(endPoint.y - sp.y) < 5.0)
      return SOUTHWEST;

    // horizontal
    if (sp.x >= startPoint.x + 5 && sp.x <= endPoint.x - 5)
    {
      if (Math.abs(startPoint.y - sp.y) < 5.0)
        return NORTH;
      if (Math.abs(endPoint.y - sp.y) < 5.0)
        return SOUTH;
    }
    // vertikal
    else if (sp.y >= startPoint.y + 5 && sp.y <= endPoint.y - 5)
    {
      if (Math.abs(startPoint.x - sp.x) < 5.0)
        return WEST;
      if (Math.abs(endPoint.x - sp.x) < 5.0)
        return EAST;
    }
    
    if(sp.x >= startPoint.x && sp.x <= endPoint.x &&
        sp.y >= startPoint.y && sp.y <= endPoint.y)
      return INREGION;   

    return NOTHING;
  }

  protected boolean isMousePosInsideImageX(Point p)
  {
    return (p.getX() >= childX && p.getX() <= childX + child.sizeX
        * properties.scaleFactor);
  }

  protected boolean isMousePosInsideImageY(Point p)
  {
    return (p.getY() >= childY && p.getY() <= childY + child.sizeY
        * properties.scaleFactor);
  }

  protected boolean isMousePosInsideImage(Point p)
  {
    return isMousePosInsideImageX(p) && isMousePosInsideImageY(p);
  }

  public void mouseDragged(MouseEvent event)
  {
    switch (mode)
    {
      case MODE_ZOOM:
        if (!dragging)
        {
          // start der auswahl
          startPoint = event.getPoint();
          dragging = true;
        }
        else
        {
          // alte auswahlbox entfernen
          if (endPoint != null)
            drawSelection(startPoint, endPoint);

          endPoint = event.getPoint();
          drawSelection(startPoint, endPoint);
        }

        repaint();
        break;
      case MODE_CROP:
        if (!dragging)
        {
          if (startPoint != null && endPoint != null)
          {
            dragging = true;
            draggedRegion = getCropRegion(event.getPoint());
            
            if (draggedRegion > NOTHING)
              return;
          }

          // start der auswahl
          if (!isMousePosInsideImage(event.getPoint()))
            return;

          drawCropSelection(startPoint, endPoint);

          startPoint = event.getPoint();
          startPoint.x = (int) Math.rint((startPoint.x - childX)
              / properties.scaleFactor);
          startPoint.y = (int) Math.rint((startPoint.y - childY)
              / properties.scaleFactor);
          endPoint = null;
          dragging = true;
          // cropSelection = new Rectangle();
        }
        else
        {
          Point currentPoint = event.getPoint();

          if (isMousePosInsideImageX(event.getPoint())
              || isMousePosInsideImageY(event.getPoint()))
          {
            drawCropSelection(startPoint, endPoint);

            currentPoint.x = (int) ((currentPoint.x - childX) / properties.scaleFactor);
            currentPoint.x = (int) Math.max(0, Math.min(child.sizeX,
                currentPoint.x));
            currentPoint.y = (int) ((currentPoint.y - childY) / properties.scaleFactor);
            currentPoint.y = (int) Math.max(0, Math.min(child.sizeY,
                currentPoint.y));
          }
          else
            return;

          int temp;

          if (draggedRegion > NOTHING)
          {
            // Point p = event.getPoint();
            int newStartX = startPoint.x, newStartY = startPoint.y, newEndX = endPoint.x, newEndY = endPoint.y;

            switch (draggedRegion)
            {
              case NORTHWEST:
                newStartX = currentPoint.x;
                newStartY = currentPoint.y;

                if (newStartY > endPoint.y && newStartX < endPoint.x)
                  draggedRegion = SOUTHEAST;
                else if (newStartX > endPoint.x)
                  draggedRegion = NORTHEAST;
                else if (newStartY > endPoint.y)
                  draggedRegion = SOUTHWEST;
//                System.out.println("NW");
                break;
              case NORTH:
                newStartY = currentPoint.y;

                if (newStartY > endPoint.y)
                  draggedRegion = SOUTH;
//                System.out.println("N");
                break;
              case NORTHEAST:
                newStartY = currentPoint.y;
                newEndX = currentPoint.x;
                
                if (newEndX < startPoint.x && newStartY > endPoint.y)
                  draggedRegion = SOUTHWEST;
                else if (newStartY > endPoint.y)
                  draggedRegion = SOUTHEAST;
                else if (newEndX < startPoint.x)
                  draggedRegion = NORTHWEST;   
//                System.out.println("NE");
                break;
              case EAST:
                newEndX = currentPoint.x;

                if (newEndX < startPoint.x)
                  draggedRegion = WEST;
//                System.out.println("E");
                break;
              case SOUTHEAST:
                newEndX = currentPoint.x;
                newEndY = currentPoint.y;

                if (newEndY < startPoint.y && newEndX < startPoint.x)
                  draggedRegion = NORTHWEST;
                else if (newEndX < startPoint.x)
                  draggedRegion = SOUTHWEST;
                else if (newEndY < startPoint.y)
                  draggedRegion = NORTHEAST;
//                System.out.println("SE");
                break;
              case SOUTH:
                newEndY = currentPoint.y;

                if (newEndY < startPoint.y)
                  draggedRegion = NORTH;
//                System.out.println("S");
                break;
              case SOUTHWEST:
                newStartX = currentPoint.x;
                newEndY = currentPoint.y;
                
                if (newEndY < startPoint.y && newStartX > endPoint.x)
                  draggedRegion = NORTHEAST;
                else if (newStartX > endPoint.x)
                  draggedRegion = SOUTHEAST;
                else if (newEndY < startPoint.y)
                  draggedRegion = NORTHWEST;        
//                System.out.println("SW");
                break;
              case WEST:
                newStartX = currentPoint.x;

                if (newStartX > endPoint.x)
                  draggedRegion = EAST;
//                System.out.println("W");
                break;
              case INREGION:
                int width = endPoint.x - startPoint.x;
                int height = endPoint.y - startPoint.y;
                // mittelpunkt mit Grenzf�llen berechnen
                int m_x = (int)Math.max(Math.ceil(width/2.0), 
                    Math.min(Math.floor(child.sizeX-width/2.0), currentPoint.x));
                int m_y = (int)Math.max(Math.ceil(height/2.0), 
                    Math.min(Math.floor(child.sizeY-height/2.0), currentPoint.y));
                
                newStartX = (int)(m_x - width/2.0);
                newStartY = (int)(m_y - height/2.0);
                newEndX = (int)(m_x + width/2.0);
                newEndY = (int)(m_y + height/2.0);
                break;
            }

            startPoint.x = newStartX;
            startPoint.y = newStartY;
            endPoint.x = newEndX;
            endPoint.y = newEndY;

//            System.out.println(startPoint + " -> " + endPoint);
            drawCropSelection(startPoint, endPoint);

            repaint();
            return;
          }

          // initales "aufziehen" der Region
          endPoint = currentPoint;

//          System.out.println(startPoint + "->" + endPoint);

          drawCropSelection(startPoint, endPoint);
        }

        repaint();
        break;
    }
  }

  public void mouseMoved(MouseEvent e)
  {
    if (isMousePosInsideImage(e.getPoint()))
    {
      if (MODE_CROP == mode && startPoint != null && endPoint != null)
      {
        // System.out.println(getCropRegion(e.getPoint()));
        switch (getCropRegion(e.getPoint()))
        {
          case NORTHWEST:
            setCursor(Cursor.getPredefinedCursor(Cursor.NW_RESIZE_CURSOR));
            break;
          case NORTH:
            setCursor(Cursor.getPredefinedCursor(Cursor.N_RESIZE_CURSOR));
            break;
          case NORTHEAST:
            setCursor(Cursor.getPredefinedCursor(Cursor.NE_RESIZE_CURSOR));
            break;
          case EAST:
            setCursor(Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR));
            break;
          case SOUTHEAST:
            setCursor(Cursor.getPredefinedCursor(Cursor.SE_RESIZE_CURSOR));
            break;
          case SOUTH:
            setCursor(Cursor.getPredefinedCursor(Cursor.S_RESIZE_CURSOR));
            break;
          case SOUTHWEST:
            setCursor(Cursor.getPredefinedCursor(Cursor.SW_RESIZE_CURSOR));
            break;
          case WEST:
            setCursor(Cursor.getPredefinedCursor(Cursor.W_RESIZE_CURSOR));
            break;
          case INREGION:
            setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
            break;
          case NOTHING:
            setCursor(currentCursor);
            break;
        }
      }
      else
        setCursor(currentCursor);
    }
    else
      setCursor(Cursor.getDefaultCursor());
  }

  public void mouseClicked(MouseEvent arg0)
  {

  }

  public void mouseEntered(MouseEvent arg0)
  {

  }

  public void mouseExited(MouseEvent arg0)
  {

  }

  public void mousePressed(MouseEvent arg0)
  {

  }

  public void mouseReleased(MouseEvent event)
  {
    switch (mode)
    {
      case MODE_ZOOM:
        if (dragging)
        {
          dragging = false;
          properties.fitToView = false;
          // alte auswahlbox entfernen
          drawSelection(startPoint, endPoint);

          // scaleFactor berechnen
          int startx = (startPoint.x < endPoint.x) ? startPoint.x - childX
              : endPoint.x - childX;
          int starty = (startPoint.y < endPoint.y) ? startPoint.y - childY
              : endPoint.y - childY;

          if (startx < 0)
            startx = 0;

          if (starty < 0)
            starty = 0;

          properties.x = (int) Math.floor(startx / properties.scaleFactor)
              + properties.x;
          properties.y = (int) Math.floor(starty / properties.scaleFactor)
              + properties.y;

          int selWidth = (int) (Math.abs(endPoint.x - startPoint.x) / properties.scaleFactor);
          int selHeight = (int) (Math.abs(endPoint.y - startPoint.y) / properties.scaleFactor);

          float scaleFactorX = (float) child.getWidth() / (float) selWidth;
          float scaleFactorY = (float) child.getHeight() / (float) selHeight;

          properties.scaleFactor = Math.min(scaleFactorX, scaleFactorY);
          clipWidth = (int) Math.floor(getWidth() / properties.scaleFactor);
          clipHeight = (int) Math.floor(getHeight() / properties.scaleFactor);

          // Grenzf�lle f�r die Position des Clips "abfangen"
          properties.x = (int) Math.max(0, Math.min(child.getWidth()
              - clipWidth, properties.x));
          properties.y = (int) Math.max(0, Math.min(child.getHeight()
              - clipHeight, properties.y));
          endPoint = null;
        }

        // updateScrollbars();
        updateOffscreen();

        break;
      case MODE_CROP:
        if (dragging)
        {
          dragging = false;
          if(draggedRegion == NOTHING)
          {
            int temp;
            if(endPoint.x < startPoint.x)
            {
              temp = endPoint.x;
              endPoint.x = startPoint.x;
              startPoint.x = temp;
            }
            
            if(endPoint.y < startPoint.y)
            {
              temp = endPoint.y;
              endPoint.y = startPoint.y;
              startPoint.y = temp;
            }
          }
          draggedRegion = NOTHING;
          
        }
        break;
    }
  }

  public void setMode(int mode)
  {
    this.mode = mode;
    if (mode == MODE_CROP)
    {
      properties.fitToView(true);
      // Auswahl initalisieren
      startPoint = null;
      endPoint = null;
      currentCursor = Cursor.getPredefinedCursor(Cursor.CROSSHAIR_CURSOR);

    }
    else
    {
      properties.fitToView(false);
      currentCursor = Cursor.getDefaultCursor();
    }

    // cropSelection = null;
    setCursor(currentCursor);
    updateOffscreen();
  }

  public int getMode()
  {
    return mode;
  }

  public Rectangle getCropSelection()
  {
    Rectangle r = new Rectangle((int) startPoint.getX(), (int) startPoint
        .getY(), (int) (endPoint.getX() - startPoint.getX()), (int) (endPoint
        .getY() - startPoint.getY()));

    return r;
  }
}
