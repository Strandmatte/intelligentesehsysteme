package imageToolBox.gui;

public class ColorImageScrollPaneProperties
{
  protected int x = 0;
  protected int y = 0;
  protected float scaleFactor = 1.0f;
  protected boolean fitToView = true;
  
  
  public void fitToView(boolean val)
  {
    fitToView = val;
  }
  
  public boolean isFitToView()
  {
    return fitToView;
  }
}
