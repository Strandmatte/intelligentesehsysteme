package imageToolBox;
public class Const
{
  static final String applicationName = "ImageToolBox";
  static final String className = "ImageToolBox.class";
  static final String tempFilenamePrefix = "temp/temp-";
  static final String configFilename = "config.txt";

  static final int defaultScreenX = 1024;
  static final int defaultScreenY = 768;

  static final int defaultSizeX = 256;
  static final int defaultSizeY = 256;

  static final int minSizeX = 660;
}
