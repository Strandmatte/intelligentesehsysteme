package imageToolBox;

import java.awt.*;
import java.awt.event.*;
import java.util.Vector;

public class PropertiesFrame extends Frame
{
  public static final String DOUBLE = "double";
  public static final String BOOLEAN = "boolean";

  public class DoubleProperty extends Container
  {
    private String name;
    TextField txtField;

    public DoubleProperty(String name)
    {
      super();
      this.name = name;
      setLayout(null);
      Label lblDesc = new Label(name + ": ");
      lblDesc.setSize(50, 25);
      lblDesc.setLocation(0,0);

      txtField = new TextField("0.0");
      txtField.setSize(50, 25);
      txtField.setLocation(55, 0);

      add(lblDesc);
      add(txtField);
      setSize(150, 25);
    }

    public double getValue()
    {
      try
      {
        return Double.parseDouble(txtField.getText());
      }
      catch (NumberFormatException e)
      {
        return 0.0;
      }
    }
    
    public void setValue(double val)
    {
      txtField.setText("" + val);
    }
    
    public String getName()
    {
      return name;
    }
  }

  public class BooleanProperty extends Container
  {
    private String name;
    Checkbox chkBox;

    public BooleanProperty(String name) 
    {
      super();
      this.name = name;
      setLayout(null);
      chkBox = new Checkbox(name);
      chkBox.setSize(100, 25);
      chkBox.setLocation(0, 0);
      add(chkBox);
      setSize(150, 25);
    }

    public boolean getValue()
    {
      return chkBox.getState();
    }
    
    public void setValue(boolean val)
    {
      chkBox.setState(val);
    }
    
    public String getName()
    {
      return name;
    }
  }
  
  protected AbstractFilter parent;
  protected Vector<Container> properties;
  protected Button b_close;
  
  protected Panel contentPane;

  public PropertiesFrame(AbstractFilter f)
  {
    super("Properties");
    properties = new Vector<Container>();
//    setLayout(null);
    setBackground(new Color(236, 233, 216));
    b_close = new Button("close");
    b_close.setSize(50,25);
    b_close.addActionListener(new ActionListener(){

      public void actionPerformed(ActionEvent arg0)
      {
        close();
      }
      
    });
    
//    add(b_close);
    
    addWindowListener(new WindowAdapter()
    {
      public void windowClosing(WindowEvent ev)
      {
        try
        {
          close();
        }
        catch (Exception ex)
        {
          ex.printStackTrace();
        }
      }
    });
    
    contentPane = new Panel();
    contentPane.setLayout(null);
    super.add(contentPane);
    super.add(b_close, BorderLayout.PAGE_END);
    setSize(100,25); // default size.
  }
  
  @Override
  public Component add(Component comp)
  {
    return contentPane.add(comp);
  }
  @Override  
  public Component add(Component comp, int i)
  {
    return contentPane.add(comp, i);
  }
  
  @Override
  public void setLayout(LayoutManager mgr)
  {
    if(contentPane == null)
      super.setLayout(mgr);
    else
      contentPane.setLayout(mgr);
  }

  public void addProperty(String type, String name)
  {
    if (type.equals(DOUBLE))
    {
      DoubleProperty property = new DoubleProperty(name);
      properties.add(property);
      add(property);
      property.setLocation(5, (properties.size()-1)*25+2*(properties.size()-1)+5);
    }
    else if (type.equals(BOOLEAN))
    {
      BooleanProperty property = new BooleanProperty(name);
      properties.add(property);
      add(property);
      property.setLocation(5, (properties.size()-1)*25+2+2*(properties.size()-1)+5);
    }
    

    setSize(125, (properties.size())*25 + 65);
//    b_close.setLocation(25+5, getHeight()-10-25);
  }

  public void close()
  {
    setVisible(false);
  }
  
  public double getDoubleProperty(String name)
  {
    for(int i = 0; i < properties.size(); i++)
    {
      Object prop = properties.get(i);
      if(prop instanceof DoubleProperty)
      {
        DoubleProperty dblProp = (DoubleProperty)prop;
        if(dblProp.getName().equals(name))
          return dblProp.getValue();
      }
    }
    
    return 0.0;
  }
  
  public boolean getBooleanProperty(String name)
  {
    for(int i = 0; i < properties.size(); i++)
    {
      Object prop = properties.get(i);
      if(prop instanceof BooleanProperty)
      {
        BooleanProperty boolProp = (BooleanProperty)prop;
        if(boolProp.getName().equals(name))
          return boolProp.getValue();
      }
    }
    
    return false;
  }
  
  public void setDoubleProperty(String name, double val)
  {
    for(int i = 0; i < properties.size(); i++)
    {
      Object prop = properties.get(i);
      if(prop instanceof DoubleProperty)
      {
        DoubleProperty dblProp = (DoubleProperty)prop;
        if(dblProp.getName().equals(name))
          dblProp.setValue(val);
      }
    }
  }
  
  public void setBooleanProperty(String name, boolean val)
  {
    for(int i = 0; i < properties.size(); i++)
    {
      Object prop = properties.get(i);
      if(prop instanceof BooleanProperty)
      {
        BooleanProperty boolProp = (BooleanProperty)prop;
        if(boolProp.getName().equals(name))
          boolProp.setValue(val);
      }
    }
  }
}
