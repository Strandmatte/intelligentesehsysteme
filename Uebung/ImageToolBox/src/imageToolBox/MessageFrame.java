package imageToolBox;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MessageFrame extends Frame implements ActionListener, Runnable
{
  Button b_close;
  MessageApplet ma;
  ImageToolBox iu;

  MessageFrame(ImageToolBox i)
  {
    iu = i;
    init();
  }

  public void init()
  {
    addWindowListener(new PrivateWindowListener());

    Panel p_north = new Panel();
    Panel p_south = new Panel();

    // ****************

    setLayout(new BorderLayout());
    p_north.setLayout(new FlowLayout());
    p_south.setLayout(new FlowLayout());

    // ****************

    Font f_title = new Font("Serif", Font.BOLD, 16);
    Label l_title = new Label("Message");
    l_title.setFont(f_title);

    b_close = new Button("Close");
    b_close.addActionListener((ActionListener) this);

    ma = new MessageApplet();

    // ****************

    p_north.add(l_title);
    p_south.add(b_close);

    // ****************

    add(p_north, "North");
    add(ma, "Center");
    add(p_south, "South");
  }

  public void open(String te, int sizeX, int sizeY)
  {
    ma.text = te;
    setBackground(new Color(236, 233, 216));
    setTitle("Message");
    setSize(sizeX, sizeY);
    Point p = iu.getLocation();
    p.translate(50, 50);
    setLocation(p);
    setVisible(true);
  }

  public void open(String te)
  {
    open(te, 200, 150);
  }

  public void openTimed(String te)
  {
    open(te, 200, 150);
    Thread t = new Thread(this);
    t.start();
  }

  public void openTimed(String te, int sizeX, int sizeY)
  {
    open(te, sizeX, sizeY);
    Thread t = new Thread(this);
    t.start();
  }

  public void run()
  {
    Utility.sleep(2);
    close();
  }

  public void close()
  {
    setVisible(false);
  }

  public void actionPerformed(ActionEvent e)
  {
    try
    {
      Object obj = e.getSource();

      if (obj instanceof Button)
      {
        if (obj == b_close)
          close();
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  class PrivateWindowListener extends WindowAdapter
  {
    PrivateWindowListener()
    {
      super();
    }

    public void windowClosing(WindowEvent ev)
    {
      close();
    }
  }
}
