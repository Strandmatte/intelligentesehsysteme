package imageToolBox;
import imageToolBox.gui.ColorImageScrollPane;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class SubFrame extends Frame implements ActionListener
{
  Button b_close;
  ImageToolBox iu;
  String name;

  public SubFrame(ImageToolBox i, String n)
  {
    super();
    iu = i;
    name = n;
    init();
  }

  public void init()
  {
    addWindowListener(new PrivateWindowListener());

    Panel p_center = new Panel();
    Panel p_south = new Panel();

    // ****************

    setLayout(new BorderLayout());
    p_center.setLayout(new FlowLayout());
    p_south.setLayout(new FlowLayout());

    // ****************

    b_close = new Button("Close");
    b_close.addActionListener((ActionListener) this);

    ColorImage ci = new ColorImage(
        iu.frameCollection.data[iu.frameCollection.actFrame].sizeX,
        iu.frameCollection.data[iu.frameCollection.actFrame].sizeY);

    int sizeX = iu.frameCollection.data[iu.frameCollection.actFrame].sizeX;
    int sizeY = iu.frameCollection.data[iu.frameCollection.actFrame].sizeY;

    int[] i = new int[sizeX * sizeY * 3];

    iu.ci.wr.getPixels(0, 0, sizeX, sizeY, i);
    ci.wr.setPixels(0, 0, sizeX, sizeY, i);

    // ****************

    add(new ColorImageScrollPane(ci), "Center");
    p_south.add(b_close);

    // ****************

    add(p_center, "North");
    add(p_south, "South");

    open();
  }

  public void open()
  {
    setBackground(new Color(236, 233, 216));
    setTitle(Const.applicationName + " - " + name);
    setSize(iu.frameCollection.data[iu.frameCollection.actFrame].sizeX + 20,
        iu.frameCollection.data[iu.frameCollection.actFrame].sizeY + 80);
    setVisible(true);
  }

  public void close()
  {
    setVisible(false);
  }

  public void actionPerformed(ActionEvent e)
  {
    try
    {
      Object obj = e.getSource();

      if (obj instanceof Button)
      {
        if (obj == b_close)
          close();
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  class PrivateWindowListener extends WindowAdapter
  {
    PrivateWindowListener()
    {
      super();
    }

    public void windowClosing(WindowEvent ev)
    {
      close();
    }
  }
}
