package imageToolBox;
import imageToolBox.gui.ColorImageScrollPaneProperties;

import java.awt.CheckboxMenuItem;
import java.io.File;

public class FrameBuffer
{
  int sizeX;
  int sizeY;
  byte[][][] data;
  File dataFile;
  ColorImageScrollPaneProperties viewProperties;
  String name;
  CheckboxMenuItem mi;
  String history;
  boolean isSelected;

  public FrameBuffer(int sx, int sy, String n, boolean isSelected)
  {
    sizeX = sx;
    sizeY = sy;
    name = n;
    this.isSelected = isSelected;
    mi = new CheckboxMenuItem(name, false);
    history = "";
    isSelected = false;

    data = new byte[3][sizeX][sizeY];
  }
  
  public void setDataFile(File df)
  {
    dataFile = df;
  }
  
  public void setViewProperties(ColorImageScrollPaneProperties prop)
  {
    viewProperties = prop;
  }
  
  public ColorImageScrollPaneProperties getViewProperties()
  {
    return viewProperties;
  }
}
