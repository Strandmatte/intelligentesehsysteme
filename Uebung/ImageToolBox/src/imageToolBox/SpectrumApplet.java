package imageToolBox;
import java.applet.Applet;
import java.awt.*;

public class SpectrumApplet extends Applet {
    SpectrumFrame sf;
        
    SpectrumApplet(SpectrumFrame s)
    {
        super();
        sf = s;
    }

    public void paint(Graphics g)
    {
        super.paint(g);
        paintSpectrum(g);
    }

    public void paint()
    {
        paint(getGraphics());
    }
    
    public void paintSpectrum(Graphics g)
    {
        g.clearRect(0, 0, 276, 285);

        int band = sf.c_band.getSelectedIndex();
        int[] c = new int[3];

        for(int i = 0; i < 256; i++) {
            if(band < 3) c[band] = i;
            else for(int b = 0; b < 3; b++) c[b] = i;
    
            g.setColor(new Color(c[0], c[1], c[2]));
            g.drawLine(i + 20, 285, i + 20, 285 - (int) sf.spectrum[band][i]);
        }
        
        g.setColor(Color.black);
        g.drawString("Peak = " + Utility.long2str(sf.max[band]), 20, 20);
    }
}
