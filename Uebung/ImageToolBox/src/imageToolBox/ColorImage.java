package imageToolBox;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

public class ColorImage extends Canvas
{
  public BufferedImage bi;
  WritableRaster wr;

  public int sizeX;
  public int sizeY;

  public ColorImage(int sx, int sy)
  {
    super();
    init(sx, sy);
  }

  public void init(int sx, int sy)
  {
    sizeX = sx;
    sizeY = sy;

    setSize(sizeX, sizeY);
    setPreferredSize(new Dimension(sizeX, sizeY));

    bi = new BufferedImage(sizeX, sizeY, BufferedImage.TYPE_INT_BGR);
    wr = bi.getRaster();

  }

//  public void paint(Graphics g)
//  {
//    g.drawImage(bi, 0, 0, null);
//  }
//
//  public void paint()
//  {
//    getGraphics().drawImage(bi, 0, 0, null);
//  }
  
  public void setImage(BufferedImage bi)
  {
    this.bi = bi;
    wr = bi.getRaster();
  }

  public void setImageData(double[][][] data)
  {
    int width = data[0].length;
    int height = data[0][0].length;

    init(width, height);

    int[] ih = new int[width * height];

    for (int b = 0; b < 3; b++)
    {
      for (int x = 0; x < width; x++)
        for (int y = 0; y < height; y++)
          ih[x + y * width] = (int) Math.rint(data[b][x][y]);
      wr.setSamples(0, 0, width, height, b, ih);
    }
    
    
  }

  public void setImageData(byte[][][] data)
  {
    int width = data[0].length;
    int height = data[0][0].length;

    init(width, height);

    int[] ih = new int[width * height];

    for (int b = 0; b < 3; b++)
    {
      for (int x = 0; x < width; x++)
        for (int y = 0; y < height; y++)
          ih[x + y * width] = Utility.byte2int(data[b][x][y]);
      wr.setSamples(0, 0, width, height, b, ih);
    }
  }
}