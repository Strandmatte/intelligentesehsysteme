package imageToolBox;

import java.awt.Component;
import java.awt.Container;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.io.*;
import java.util.HashMap;

/**
 * Abstrakter Filter als Basis-Klasse f�r selbstimplementierte Filter
 * 
 * Bietet die M�glichkeit Daten zu speichern und somit anderen Filtern zur
 * Verf�gung zustellen.
 * 
 * Ausserdem sind einige "Helfer"-Funktionen implementiert.
 * 
 * @author jens behley
 */
public class AbstractFilter
{
  protected int width;
  protected int height;
  
  protected File[] srcData;
  protected BufferedReader[] brSrcData; 
  protected File data;
  protected BufferedWriter bw;
  protected BufferedReader br;
  
  protected PropertiesFrame propFrame;

  public final boolean saveImage(double[][][] img, String filename) {
	  OutputStream os = null;
	  try {
		  if (!filename.endsWith(".ppm")) {
			  filename += ".ppm";
		  }
		  os = new FileOutputStream(filename);
		  os.write("P6\n".getBytes());
		  os.write("# Created by ImageToolBox\n".getBytes());
		  os.write((width + " " + height + "\n").getBytes());
		  os.write("255\n".getBytes());
		  for (int y=0; y<height; y++) {
			  for (int x=0; x<width;x++) {
				  os.write(Utility.int2byte((int)img[0][x][y]));
				  os.write(Utility.int2byte((int)img[1][x][y]));
				  os.write(Utility.int2byte((int)img[2][x][y]));
			  }
		  }
		  return true;
	  } catch (IOException e) {
		  return false;
	  } finally {
		  try {
			  os.close();
		  } catch (Exception e)  { /* do nothing */ }
	  }
  }
  
  /**
   * tempor�re Datei erzeugen, die nach dem Beenden
   * der Applikation wieder entfernt wird 
   *
   */
  public final void createDatafile()
  {
    createDatafile(null, true);
  }

  /**
   * Datei mit filename als Namen erzeugen, die nach 
   * dem Beenden der Applikation wieder entfernt wird
   * 
   */
  public final void createDatafile(String filename)
  {
    createDatafile(filename, true);
  }

  /**
   * Tempor�rdatei erzeugen mit filename als Dateinamen.
   * @param filename        Dateiname
   * @param deleteOnExit    nach Beenden entfernen?
   */
  public final void createDatafile(String filename, boolean deleteOnExit)
  {
    try
    {
      if(filename == null)
        data = File.createTempFile("dat", ".txt");
      else
      {
        data = new File(filename);
        if(data.exists())
          data.delete();
        data = new File(filename);
      }
      
      if(deleteOnExit)
        data.deleteOnExit();
    }
    catch (IOException e)
    {
      System.err.println("Unable to create temporary file.");
    }
  }
  
  /**
   * String an Datei anh�ngen
   * @param s   
   */
  public final void write(String s)
  {
    try
    {
      if (bw == null)
      {
        bw = new BufferedWriter(new FileWriter(data));
      }
      
      bw.write(s);
    }
    catch (IOException e)
    {
      System.err.println("Unable to write line in temporary file.");
    }
  }
  
  /**
   * Zeile an Tmepor�rdatei anh�ngen
   * @param s   line as string
   */
  public final void writeLine(String s)
  {
    try
    {
      if (bw == null)
      {
        bw = new BufferedWriter(new FileWriter(data));
      }
      
      bw.write(s + "\n");
    }
    catch (IOException e)
    {
      System.err.println("Unable to write line in temporary file.");
    }
  }
    
  /**
   * Zeile aus Tempor�rdatei lesen
   * @param s   line as string
   */
  public final String readLine()
  {
    try
    {
      if (br == null)
      {
        br = new BufferedReader(new FileReader(data));
      }
      
      return br.readLine();
    }
    catch (IOException e)
    {
      System.err.println("Unable to read a line from temporary file.");
      
      return null;
    }
  }
  
  public final void close()
  {
    try
    {
      if(bw != null)
      {
        bw.close();
        bw = null;
      }
      
      if(br != null)
      {
        br.close();
        br = null;
      }
      
      if(brSrcData != null)
      {
        for(int i = 0; i < brSrcData.length; i++)
        {
          if(brSrcData[i] != null)
            brSrcData[i].close();
        }
        brSrcData = null;
      }
    }
    catch(IOException e)
    {
      System.err.println("Unable to close filehandles!");
    }
  }
  
  public final File getDatafile()
  {
    return data;
  }

  /**
   * Dimensionen des Eingabebilds setzen
   * 
   * @param width Breite des Eingabebilds
   * @param height H�he des Eingabebilds
   */
  public final void setDimensions(int width, int height)
  {
    this.width = width;
    this.height = height;
  }
  
  public final void setSrcData(File... files)
  {    
    try
    {
      if(brSrcData != null)
      {
        for(int i = 0; i < brSrcData.length; i++)
        {
          if(brSrcData[i] != null)
            brSrcData[i].close();
        }
        brSrcData = null;
      }
      
      if(files == null)
        return;
      
      srcData = files;
      brSrcData = new BufferedReader[files.length];
      for(int i = 0; i < files.length; i++)
      {
        if(srcData[i] != null)
          brSrcData[i] = new BufferedReader(new FileReader(srcData[i]));
      }
    }
    catch(IOException e)
    {
      System.err.println("Unable to create BufferReaders for src Data.");
    }
  }
  
  /**
   * Zeile der Tempor�rdatei des Eingabebildes lesen
   * @return Zeile der Tempor�rdatei
   */
  public final String readSrcDataLine()
  {
    return readSrcDataLine(0);
  }
  
  /**
   * Zeile der Tempor�rdatei des iten Eingabebildes lesen
   * @param i   ite Tempor�rdatei auslesen
   * 
   * @return Zeile der Tempor�rdatei
   */
  public final String readSrcDataLine(int i)
  {
    try
    {
      if(brSrcData != null)
      {
        if(brSrcData[i] != null)
          return brSrcData[i].readLine();
      }
      
      return null;
    }
    catch(IOException e)
    {
      System.err.println("Unable to read a line from SrcData.");
      
      return null;
    }
  }
  
  
  // ====== Helfer-Funktionen ======
  
  /**
   * Konvertierung eines Farbbildes in Graustufenbild
   * 
   * [grauwert = 0.299*R + 0.5870*G + 0.1140*B]
   */
  public final void toGreyscale(double[][][] src)
  {
    for(int i = 0; i < src[0].length; i++)
      for(int j = 0; j < src[0][0].length; j++)
      {
        double greyvalue = 0.299 * src[0][i][j] + 
                           0.5870 * src[1][i][j] + 
                           0.1140 * src[2][i][j];
        for(int b = 0; b < 3; b++)
          src[b][i][j] = greyvalue;
      }
  }

  /**
   * zu implementierende Filterfunktion
   * 
   * @param src Eingabebild
   * @param dst Ausgabebild
   */
  public void filter(double[][][] src, double[][][] dst)
  {

  }

  /**
   * zu implementierende Filterfunktion mit zwei Eingabebildern
   * 
   * @param src1 Eingabebild1
   * @param src2 Eingabebild2
   * @param dst Ausgabebild
   */
  public void filter(double[][][] src1, double[][][] src2, double[][][] dst)
  {

  }
  
  
  /**
   * Hat Filter Properties?
   * @return
   */
  public boolean hasProperties()
  {
    return false;
  }
  
  public void handleMouseClick(Point p) {
	  
  }
  
  /**
   * Funktion zum initalisieren der GUI Elemente; diese Methode
   * muss �berschrieben werden um dem PropertiesFrame Elemente hinzuzuf�gen.
   *
   */
  public void initGUI()
  {
    
  }
  
  public final Frame getPropertiesFrame()
  {
    return propFrame;
  }
  
  public final void initPropertiesFrame()
  {
    if(hasProperties() && propFrame == null)
    {
      propFrame = new PropertiesFrame(this);
      initGUI();
    }
  }
  
  public final void addDoubleProperty(String name)
  {
    addDoubleProperty(name, 0.0);
  }
  
  public final void addDoubleProperty(String name, double value)
  {
    propFrame.addProperty(PropertiesFrame.DOUBLE, name);
    propFrame.setDoubleProperty(name, value);
  }
  
  public final double getDoubleProperty(String name)
  {
    return propFrame.getDoubleProperty(name);
  }
  
  public final void addBooleanProperty(String name)
  {
    addBooleanProperty(name, false);
  }
  
  public final void addBooleanProperty(String name, boolean value)
  {
    propFrame.addProperty(PropertiesFrame.BOOLEAN, name);
    propFrame.setBooleanProperty(name, value);
  }
  
  public final boolean getBooleanProperty(String name)
  {
    return propFrame.getBooleanProperty(name);
  }
}
