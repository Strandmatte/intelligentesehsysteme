package imageToolBox;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Point;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class SpectrumPropertiesFrame extends Frame
implements ActionListener {
  Button b_close;
  TextField[] tf_filter;
  TextField tf_times;
  SpectrumFrame sf;

  public SpectrumPropertiesFrame(SpectrumFrame s)
  {
      super();
      sf = s;
      init();
  }

  public void init()
  {
      addWindowListener(new PrivateWindowListener());

      Panel p_north = new Panel();
      Panel p_center = new Panel();
      Panel p_south = new Panel();

      // ****************

      setLayout(new BorderLayout());
      p_north.setLayout(new FlowLayout());
      p_center.setLayout(new FlowLayout());
      p_south.setLayout(new FlowLayout());

      // ****************

      Font f_title = new Font("Serif", Font.BOLD, 16);
      Label l_title = new Label("Smooth");
      l_title.setFont(f_title);

      Label l_filter = new Label("Filter");
      Label l_times = new Label("Times");

      b_close = new Button("Close");
      b_close.addActionListener((ActionListener) this);

      tf_filter = new TextField[3];
      for(int i = 0; i < 3; i++) tf_filter[i] = new TextField("1.0");
      tf_times = new TextField("5");
          
      // ****************

      add(p_north, "North");
      add(p_center, "Center");
      add(p_south, "South");

      p_north.add(l_title);

      p_center.add(l_filter);
      for(int i = 0; i < 3; i++) p_center.add(tf_filter[i]);
      p_center.add(l_times);
      p_center.add(tf_times);

      p_south.add(b_close);
  }

  public void open()
  {
      setBackground(new Color(236, 233, 216));
      setTitle("Smooth");
      setSize(230, 180);
      Point p = sf.getLocation();
      p.translate(50, 50);
      setLocation(p);
      setVisible(true);
  }

  public void close()
  {
      setVisible(false);
  }

  public void actionPerformed(ActionEvent e)
  {
      try {
          Object obj = e.getSource();

          if(obj instanceof Button) {
              if(obj == b_close) {
                  close();
                  sf.update();
              }
          }
      }
      catch(Exception ex) {
          ex.printStackTrace();
      }
  }

  class PrivateWindowListener extends WindowAdapter {
      PrivateWindowListener()
      {
          super();
      }

      public void windowClosing(WindowEvent ev)
      {
          close();
      }
  }
}
