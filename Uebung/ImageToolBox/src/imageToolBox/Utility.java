package imageToolBox;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;

public class Utility {
	
	public static void safeMkdirs(File folder) throws IOException {
		if (folder.isDirectory())
			return;

		if (folder.exists())
			throw new IOException(folder + " exists but is no directory.");

		if (!folder.mkdirs()) {
			throw new IOException("failed to create directory '" + folder + "'");
		}
	}

	public static void closeQuietly(Closeable... closeables) {
		for (Closeable closeable : closeables) {
			if (closeable != null) {
				try {
					closeable.close();
				} catch (IOException e) {
					// ignore
				}
			}
		}
	}

	public static String int2str(int i) {
		return String.valueOf(i);
	}

	public static String long2str(long l) {
		return String.valueOf(l);
	}

	public static int str2int(String str) {
		return Integer.valueOf(str).intValue();
	}

	public static double str2double(String str) {
		return Double.valueOf(str).doubleValue();
	}

	public static String double2str(double d) {
		return new Double(d).toString();
	}

	// ****************

	public static int byte2int(byte b) {
		if (b < 0)
			return (int) (b + 256);
		else
			return (int) b;
	}

	public static byte int2byte(int i) {
		if (i < 0)
			return (byte) 0;
		else if (i > 255)
			return (byte) 255;
		else
			return (byte) i;
	}

	// ****************

	public static double byte2double(byte b) {
		if (b < 0)
			return (double) (b + 256);
		else
			return (double) b;
	}

	public static byte double2byte(double d) {
		if (d < 0.0)
			return (byte) 0;
		else if (d > 255.0)
			return (byte) 255;
		else
			return (byte) d;
	}

	public static float normFloat(float f) {
		if (f < 0.0)
			return (float) 0.0;
		else if (f > 255.0)
			return (float) 255.0;
		else
			return f;
	}

	public static void sleep(int t) {
		try {
			Thread.sleep(1000 * t);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
}
