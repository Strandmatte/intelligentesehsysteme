package imageToolBox;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.lang.reflect.*;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.filechooser.FileFilter;

import imageToolBox.commands.*;
import imageToolBox.gui.*;

public final class ImageToolBox extends Frame implements // ActionListener,
    ItemListener, Runnable, MouseListener
{
  /**
   * 
   */
  private static final long serialVersionUID = 4473084279122157360L;
  Button b_exit;
  Button b_loadImage;
  Button b_deleteImage;
  Button b_saveImage;
  Button b_loadFilter;
  Button b_applyFilter;
  Button b_nextImage;
  Button b_prevImage;
  Button b_showImage;
  Button b_filterProperties;
  Button b_spectrum;
  Button b_originalSize;

  Menu m_image;
  Menu m_filter;
  MenuItem mi_loadImage;
  MenuItem mi_saveImage;
  MenuItem mi_deleteImage;
  MenuItem mi_nextImage;
  MenuItem mi_prevImage;
  MenuItem mi_merge;
  MenuItem mi_spectrum;
  MenuItem mi_showImage;
  MenuItem mi_loadFilter;
  MenuItem mi_filterProperties;
  MenuItem mi_applyFilter;
  MenuItem mi_deleteFilter;
  MenuItem mi_info;
  MenuItem mi_exit;
  MenuItem mi_frameSeparator;
  MenuItem mi_filterSeparator;
  MenuItem mi_fitToScreen;
  MenuItem mi_fullSize;
  CheckboxMenuItem mi_cropImage;

  Panel p_north;
  Panel p_center;
  Panel p_south;

  ColorImage ci;
  ColorImageScrollPane scrollpane;
  FrameCollection frameCollection;
  FilterCollection filterCollection;
  JFileChooser jfc_image;
  // FileDialog fd_saveImage;
  FileDialog fd_loadFilter;
  SpectrumFrame spectrumFrame;
  MessageFrame messageFrame;
  MergeFrame mergeFrame;
  Checkbox cb_isSelected;

  ClassLoader classLoader;
  Object loadedClassInstance;

  volatile int method;

  public static final int methodApplyFilter = 0;
  public static final int methodFilterProperties = 1;

  static Frame frame;

  // ****************

  public void run()
  {
    if (method == methodApplyFilter)
      applyFilter();
  }

  public static void main(String[] args)
  {
    new ImageToolBox();
  }

  public static Frame getFrame()
  {
    return frame;
  }

  public ImageToolBox()
  {
    super();
    frame = this;
    create();
  }

  void create()
  {
    PrivateWindowListener listener = new PrivateWindowListener();
    addWindowListener(listener);

    p_north = new Panel();
    p_south = new Panel();

    // ****************

    setLayout(new BorderLayout());

    p_north.setLayout(new FlowLayout());

    ci = new ColorImage(Const.defaultSizeX, Const.defaultSizeY);
    scrollpane = new ColorImageScrollPane(ci);
    scrollpane.addMouseListener(this);
    p_south.setLayout(new FlowLayout());

    // ********

    Font f_title = new Font("Serif", Font.BOLD, 16);

    Label l_title = new Label(Const.applicationName);
    l_title.setFont(f_title);

    Label l_image = new Label("Image:");
    Label l_filter = new Label("Filter:");

    Command cmdExit = new ExitCmd(this);
    Command cmdLoadImage = new LoadImageCmd(this);
    Command cmdDeleteImage = new DeleteImageCmd(this);
    Command cmdSaveImage = new SaveImageCmd(this);
    Command cmdLoadFilter = new LoadFilterCmd(this);
    Command cmdApplyFilter = new ApplyFilterCmd(this);
    Command cmdDeleteFilter = new DeleteFilterCmd(this);
    Command cmdNextImage = new NextImageCmd(this);
    Command cmdPrevImage = new PrevImageCmd(this);
    Command cmdMergeImage = new MergeImageCmd(this);
    Command cmdShowImage = new ShowImageCmd(this);
    Command cmdFilterProperties = new FilterPropertiesCmd(this);
    Command cmdSpectrum = new SpectrumCmd(this);
    Command cmdShowInfo = new ShowInfoCmd(this);
    Command cmdOrgSize = new OriginalSizeCmd(this);
    Command cmdFitToScreen = new FitToScreenCmd(this);
    Command cmdCropImage = new CropImageCmd(this);

    b_exit = new CommandButton("Exit", cmdExit);
    b_loadImage = new CommandButton("Load", cmdLoadImage);
    b_deleteImage = new CommandButton("Delete", cmdDeleteImage);
    b_saveImage = new CommandButton("Save", cmdSaveImage);
    b_loadFilter = new CommandButton("Load", cmdLoadFilter);
    b_applyFilter = new CommandButton("Apply", cmdApplyFilter);
    b_nextImage = new CommandButton("Next", cmdNextImage);
    b_prevImage = new CommandButton("Prev", cmdPrevImage);
    b_showImage = new CommandButton("[]", cmdShowImage);
    b_filterProperties = new CommandButton("Properties", cmdFilterProperties);
    b_spectrum = new CommandButton("Spectrum", cmdSpectrum);

    b_originalSize = new CommandButton("1:1", cmdOrgSize);

    cb_isSelected = new Checkbox(" ", false);
    cb_isSelected.addItemListener(this);

    MenuBar mb = new MenuBar();
    Menu m_main = new Menu("Main");
    m_image = new Menu("Image");
    m_filter = new Menu("Filter");
    Menu m_help = new Menu("Help");

    mi_exit = new CommandMenuItem("Exit", cmdExit);

    mi_loadImage = new CommandMenuItem("Load", cmdLoadImage);
    mi_saveImage = new CommandMenuItem("Save", cmdSaveImage);
    mi_deleteImage = new CommandMenuItem("Delete", cmdDeleteImage);
    mi_nextImage = new CommandMenuItem("Next", cmdNextImage);
    mi_prevImage = new CommandMenuItem("Previous", cmdPrevImage);
    mi_merge = new CommandMenuItem("Merge", cmdMergeImage);
    mi_spectrum = new CommandMenuItem("Spectrum", cmdSpectrum);
    mi_showImage = new CommandMenuItem("Show", cmdShowImage);
    mi_cropImage = new CommandCheckboxMenuItem("Crop", cmdCropImage);
    mi_fullSize = new CommandMenuItem("Full-Size", cmdOrgSize);
    mi_fitToScreen = new CommandMenuItem("Fit to Screen", cmdFitToScreen);

    mi_loadFilter = new CommandMenuItem("Load", cmdLoadFilter);
    mi_filterProperties = new CommandMenuItem("Properties", cmdFilterProperties);
    mi_applyFilter = new CommandMenuItem("Apply", cmdApplyFilter);
    mi_deleteFilter = new CommandMenuItem("Delete", cmdDeleteFilter);

    mi_info = new CommandMenuItem("Info", cmdShowInfo);

    mi_frameSeparator = new MenuItem("-");
    mi_filterSeparator = new MenuItem("-");

    m_main.add(mi_exit);

    m_image.add(mi_loadImage);
    m_image.add(mi_saveImage);
    m_image.add(mi_deleteImage);
    m_image.add(new MenuItem("-"));
    m_image.add(mi_nextImage);
    m_image.add(mi_prevImage);
    m_image.add(new MenuItem("-"));
    m_image.add(mi_cropImage);
    m_image.add(mi_merge);
    m_image.add(mi_spectrum);
    m_image.add(mi_showImage);
    m_image.add(new MenuItem("-"));
    m_image.add(mi_fullSize);
    m_image.add(mi_fitToScreen);

    m_filter.add(mi_loadFilter);
    m_filter.add(mi_deleteFilter);
    m_filter.add(new MenuItem("-"));
    m_filter.add(mi_filterProperties);
    m_filter.add(mi_applyFilter);

    m_help.add(mi_info);

    mb.add(m_main);
    mb.add(m_image);
    mb.add(m_filter);
    mb.add(m_help);
    setMenuBar(mb);

    // ****************

    // p_north.add(l_title);

    // p_center.add(ci);

    p_south.add(l_image);
    p_south.add(b_loadImage);
    p_south.add(b_deleteImage);
    p_south.add(b_saveImage);
    p_south.add(b_prevImage);
    p_south.add(b_nextImage);
    p_south.add(b_spectrum);
    p_south.add(b_showImage);
    p_south.add(cb_isSelected);
    // p_south.add(b_originalSize);

    p_south.add(l_filter);
    p_south.add(b_loadFilter);
    p_south.add(b_filterProperties);
    p_south.add(b_applyFilter);

    p_south.add(b_exit);

    // ****************

    add(p_north, "North");
    add(scrollpane, "Center");
    add(p_south, "South");

    // ****************

    frameCollection = new FrameCollection(this);
    filterCollection = new FilterCollection(this);
    jfc_image = new JFileChooser("image");

    // Look & Feel SWING
    try
    {
      UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
    }
    catch (ClassNotFoundException e)
    {
      System.err.println("Using the default look and feel.");
    }
    catch (UnsupportedLookAndFeelException e)
    {
      System.err.println("Using the default look and feel.");
    }
    catch (Exception e)
    {
      System.err.println("Using the default look and feel.");
    }

    jfc_image.updateUI();

    jfc_image.addChoosableFileFilter(new FileFilter()
    {
      public boolean accept(File f)
      {
        String name = f.getAbsolutePath();
        if (f.isDirectory())
          return true;
        else if (name.endsWith(".ppm") || name.endsWith(".bmp")
            || name.endsWith(".jpg") || name.endsWith(".png"))
          return true;

        return false;
      }

      public String getDescription()
      {

        return "images (*.ppm, *.bmp, *.jpg, *.png)";
      }

    });

    fd_loadFilter = new FileDialog(this, "Load Filter");
    fd_loadFilter.setDirectory("filter");

    classLoader = getClass().getClassLoader();
    loadedClassInstance = null;

    spectrumFrame = new SpectrumFrame(this);
    messageFrame = new MessageFrame(this);
    mergeFrame = new MergeFrame(this);

    setBackground(new Color(236, 233, 216));

    setSizeLocationAndShow(ci.sizeX, ci.sizeY);

    messageFrame.open("Loading data.\nPlease wait.");
    readConfig();
    messageFrame.close();

    if (frameCollection.nFrames == 0 && filterCollection.nFilters == 0)
    {
      setTitle(Const.applicationName);
    }

    setSizeLocationAndShow(ci.sizeX, ci.sizeY);

  }

  public void itemStateChanged(ItemEvent e)
  {
    Object obj = e.getSource();

    if (obj instanceof CheckboxMenuItem)
    {
      for (int i = 0; i < frameCollection.nFrames; i++)
      {
        if (obj == frameCollection.data[i].mi)
          setImage(i);
      }

      for (int i = 0; i < filterCollection.nFilters; i++)
      {
        if (obj == filterCollection.data[i].mi)
          setFilter(i);
      }
    }
    else if (obj instanceof Checkbox)
    {
      if (obj == cb_isSelected)
      {
        if (frameCollection.nFrames == 0)
          cb_isSelected.setState(false);
        else
        {
          if (cb_isSelected.getState())
          {
            for (int i = 0; i < frameCollection.nFrames; i++)
              frameCollection.data[i].isSelected = false;
            frameCollection.data[frameCollection.actFrame].isSelected = true;
          }
          else
          {
            frameCollection.data[frameCollection.actFrame].isSelected = false;
          }
        }
      }
    }
  }

  public void setTitleImageFilter()
  {
    FrameCollection frc = frameCollection;
    FilterCollection fic = filterCollection;

    if (frc.nFrames > 0 && fic.nFilters > 0)
      setTitle(Const.applicationName + " - " + frc.actFrame + " - "
          + frc.data[frc.actFrame].name + " - " + fic.data[fic.actFilter].name);
    else if (frc.nFrames > 0 && fic.nFilters == 0)
      setTitle(Const.applicationName + " - " + frc.actFrame + " - "
          + frc.data[frc.actFrame].name);
    else if (frc.nFrames == 0 && fic.nFilters > 0)
      setTitle(Const.applicationName + " - " + fic.data[fic.actFilter].name);
    else
      setTitle(Const.applicationName);
  }

  public void loadImage()
  {
    try
    {
      int returnVal = jfc_image.showOpenDialog(this);
      if (returnVal == JFileChooser.APPROVE_OPTION && jfc_image.getSelectedFile() != null)
      {
        String filename = jfc_image.getSelectedFile().getPath();

        int index = filename.indexOf(".");
        String name;
        if (index != -1)
          name = filename.substring(0, index);
        else
          name = filename;

        if (filename.endsWith(".ppm"))
          loadPpm(filename, name);
        else if (filename.endsWith(".jpg") || filename.endsWith(".png")
            || filename.endsWith(".bmp"))
          loadImageFile(filename, name);
        else
          messageFrame.openTimed("Unsupported image type.");

        spectrumFrame.update();
      }
    }
    catch (FileNotFoundException ex)
    {
      messageFrame.openTimed("Image not found.");
      // ex.printStackTrace();
    }
    catch (Exception ex)
    {
      messageFrame.openTimed("Unsupported image type.");
//      ex.printStackTrace();
    }
  }

  public void saveImage()
  {
    try
    {
      if (frameCollection.actFrame >= 0)
      {
        // fd_saveImage.setVisible(true);
        int returnVal = jfc_image.showSaveDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION)
        {

          String filename = jfc_image.getSelectedFile().getPath();
          // String directory = fd_saveImage.getDirectory();
          int index = filename.indexOf(".ppm");

          if (index != -1 && index + 4 == filename.length())
          {
            savePpm(filename);
            messageFrame.openTimed("Image saved.");
          }

          if (filename.endsWith(".ppm"))
            savePpm(filename);
          else if (filename.endsWith(".jpg") || filename.endsWith(".png")
              || filename.endsWith(".bmp"))
            saveImageFile(filename);

          messageFrame.openTimed("Image saved.");
        }
      }
      else
        messageFrame.openTimed("No image loaded.");
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public void prevImage()
  {
    mi_cropImage.setState(false);
    frameCollection.prev();
  }

  public void nextImage()
  {
    mi_cropImage.setState(false);
    frameCollection.next();
  }

  public void deleteImage()
  {
    frameCollection.delete();
  }

  public void loadFilter()
  {
    try
    {
      fd_loadFilter.setVisible(true);
      if (fd_loadFilter.getFile() != null)
      {
        // String className = fd_loadFilter.getDirectory() +
        // fd_loadFilter.getFile();
        String className = fd_loadFilter.getFile();
        int index = className.indexOf(".class");
        if (index != -1 && index + 6 == className.length())
        {
          className = className.substring(0, index);
          int i = filterCollection.loaded(className);
          if (i != -1)
          {
            filterCollection.set(i);
            messageFrame
                .openTimed("Filter already loaded.\nSwitching to filter.");
            return;
          }

          classLoader.clearAssertionStatus();
          loadedClassInstance = classLoader.loadClass(className).newInstance();
        }
        else
          throw new Exception("NotAJavaClassException");

        if (loadedClassInstance != null)
        {
          if (loadedClassInstance instanceof AbstractFilter)
          {
            ((AbstractFilter) loadedClassInstance).initPropertiesFrame();
            filterCollection.insert(loadedClassInstance, className, className);
            messageFrame.openTimed("Filter loaded.");
          }
          else
          {

            final Class[] emptyC = {};
            final Object[] empty = {};

            Class clss = loadedClassInstance.getClass();
            Method method = clss.getMethod("getID", emptyC);
            String str = (String) method.invoke(loadedClassInstance, empty);

            if (str.equals("<id>ImageToolBox</id>"))
            {
              filterCollection
                  .insert(loadedClassInstance, className, className);
              messageFrame.openTimed("Filter loaded.");
            }
            else
              throw new Exception("UnsupportedFilterTypeException");
          }
        }
      }
    }
    catch (ClassNotFoundException ex)
    {
      messageFrame.openTimed("Filter not found.");
      if (filterCollection.nFilters > 0)
        filterCollection.set(filterCollection.actFilter);
      else
        loadedClassInstance = null;
      // ex.printStackTrace();
    }
    catch (Exception ex)
    {
      if (ex.getMessage() != null)
      {
        if (ex.getMessage().equals("NotAJavaClassException"))
          messageFrame.openTimed("Not a JAVA class.");
      }
      else
        messageFrame.openTimed("Unsupported filter type.");

      if (filterCollection.nFilters > 0)
        filterCollection.set(filterCollection.actFilter);
      else
        loadedClassInstance = null;
      ex.printStackTrace();
    }
  }

  public void applyFilter()
  {
    try
    {
      if (loadedClassInstance instanceof AbstractFilter
          && frameCollection.nFrames > 0)
      {
        AbstractFilter filter = (AbstractFilter) loadedClassInstance;
        int actFrame = frameCollection.actFrame;
        int sizeX = frameCollection.data[actFrame].sizeX;
        int sizeY = frameCollection.data[actFrame].sizeY;
        String name = frameCollection.data[actFrame].name;

        int dstSizeX = sizeX;
        int dstSizeY = sizeY;

        frameCollection.data[frameCollection.nFrames] = new FrameBuffer(
            dstSizeX, dstSizeY, name + "-F", false);

        m_image.add(frameCollection.data[frameCollection.nFrames].mi);
        frameCollection.data[frameCollection.nFrames].mi
            .addItemListener((ItemListener) this);

        byte[][][] frameDstByte = frameCollection.data[frameCollection.nFrames].data;
        byte[][][] frameSrcByte = frameCollection.data[frameCollection.actFrame].data;

        double[][][] frameDst = new double[3][dstSizeX][dstSizeY];
        double[][][] frameSrc = new double[3][sizeX][sizeY];

        for (int b = 0; b < 3; b++)
        {
          for (int x = 0; x < sizeX; x++)
            for (int y = 0; y < sizeY; y++)
            {
              frameSrc[b][x][y] = Utility.byte2double(frameSrcByte[b][x][y]);
            }
        }

        filter.setDimensions(sizeX, sizeY);
        filter.setSrcData(frameCollection.data[actFrame].dataFile);

        filter.filter(frameSrc, frameDst);

        filter.close();

        frameCollection.data[frameCollection.nFrames].setDataFile(filter
            .getDatafile());

        for (int b = 0; b < 3; b++)
        {
          for (int x = 0; x < dstSizeX; x++)
            for (int y = 0; y < dstSizeY; y++)
            {
              frameDstByte[b][x][y] = Utility.double2byte(frameDst[b][x][y]);
            }
        }

        frameCollection.data[frameCollection.actFrame].mi.setState(false);
        frameCollection.actFrame = frameCollection.nFrames;
        actFrame = frameCollection.actFrame;
        frameCollection.data[frameCollection.actFrame].mi.setState(true);

        ci.setImageData(frameDst);

        setTitleImageFilter();
        
        ColorImageScrollPaneProperties prop = new ColorImageScrollPaneProperties();
        prop.fitToView(true);
        frameCollection.setViewProperties(frameCollection.actFrame, prop);
        
        scrollpane.setProperties(prop);
        scrollpane.repaint();
        // setSizeAndShow(dstSizeX, dstSizeY);
        cb_isSelected.setState(false);
        frameCollection.nFrames++;
        spectrumFrame.update();
        messageFrame.openTimed("Image filtered.");
      }
      else if (loadedClassInstance != null && frameCollection.nFrames > 0)
      {

        boolean expand = false;
        boolean use2SrcFrames = false;
        int srcFrame2 = -1;

        try
        {
          final Class[] emptyC = {};
          final Object[] empty = {};

          Class clss = loadedClassInstance.getClass();
          Method method = clss.getMethod("getMode", emptyC);
          String str = (String) method.invoke(loadedClassInstance, empty);
          if (str.equals("expand=true"))
            expand = true;
        }
        catch (Exception ex)
        {
        }

        try
        {
          final Class[] emptyC = {};
          final Object[] empty = {};

          Class clss = loadedClassInstance.getClass();
          Method method = clss.getMethod("getNSource", emptyC);
          String str = (String) method.invoke(loadedClassInstance, empty);
          if (str.equals("<nsource>2</nsource>"))
          {
            use2SrcFrames = true;
          }
        }
        catch (Exception ex)
        {
        }

        int actFrame = frameCollection.actFrame;
        int sizeX = frameCollection.data[actFrame].sizeX;
        int sizeY = frameCollection.data[actFrame].sizeY;
        String name = frameCollection.data[actFrame].name;

        int sizeD = (int) Math.sqrt(sizeX * sizeX + sizeY * sizeY) + 2;
        int dstSizeX, dstSizeY;

        if (expand)
        {
          dstSizeX = sizeD;
          dstSizeY = sizeD;
        }
        else
        {
          dstSizeX = sizeX;
          dstSizeY = sizeY;
        }

        if (use2SrcFrames)
        {
          srcFrame2 = -1;
          for (int i = 0; i < frameCollection.nFrames; i++)
            if (frameCollection.data[i].isSelected)
            {
              srcFrame2 = i;
              break;
            }
          if (srcFrame2 == -1)
            throw new Exception("SourceFrame2Exception");
        }

        frameCollection.data[frameCollection.nFrames] = new FrameBuffer(
            dstSizeX, dstSizeY, name + "-F", false);

        m_image.add(frameCollection.data[frameCollection.nFrames].mi);
        frameCollection.data[frameCollection.nFrames].mi
            .addItemListener((ItemListener) this);

        byte[][][] frameDstByte = frameCollection.data[frameCollection.nFrames].data;
        byte[][][] frameSrcByte = frameCollection.data[frameCollection.actFrame].data;

        double[][][] frameDst = new double[3][dstSizeX][dstSizeY];
        double[][][] frameSrc = new double[3][sizeX][sizeY];

        byte[][][] frameSrc2Byte = null;
        double[][][] frameSrc2 = null;
        int src2SizeX, src2SizeY;

        if (use2SrcFrames)
        {
          src2SizeX = frameCollection.data[srcFrame2].sizeX;
          src2SizeY = frameCollection.data[srcFrame2].sizeY;

          frameSrc2Byte = frameCollection.data[srcFrame2].data;
          frameSrc2 = new double[3][src2SizeX][src2SizeY];

          for (int b = 0; b < 3; b++)
          {
            for (int x = 0; x < src2SizeX; x++)
              for (int y = 0; y < src2SizeY; y++)
              {
                frameSrc2[b][x][y] = Utility
                    .byte2double(frameSrc2Byte[b][x][y]);
              }
          }
        }

        for (int b = 0; b < 3; b++)
        {
          for (int x = 0; x < sizeX; x++)
            for (int y = 0; y < sizeY; y++)
            {
              frameSrc[b][x][y] = Utility.byte2double(frameSrcByte[b][x][y]);
            }
        }

        if (!use2SrcFrames)
        {
          Class[] classType = new Class[2];
          classType[0] = frameSrc.getClass();
          classType[1] = frameDst.getClass();

          Object[] parameter = new Object[2];
          parameter[0] = frameSrc;
          parameter[1] = frameDst;

          Class clss = loadedClassInstance.getClass();
          Method method = clss.getMethod("filter", classType);
          method.invoke(loadedClassInstance, parameter);
        }
        else
        {
          Class[] classType = new Class[3];
          classType[0] = frameSrc.getClass();
          classType[1] = frameSrc2.getClass();
          classType[2] = frameDst.getClass();

          Object[] parameter = new Object[3];
          parameter[0] = frameSrc;
          parameter[1] = frameSrc2;
          parameter[2] = frameDst;

          Class clss = loadedClassInstance.getClass();
          Method method = clss.getMethod("filter", classType);
          method.invoke(loadedClassInstance, parameter);
        }

        for (int b = 0; b < 3; b++)
        {
          for (int x = 0; x < dstSizeX; x++)
            for (int y = 0; y < dstSizeY; y++)
            {
              frameDstByte[b][x][y] = Utility.double2byte(frameDst[b][x][y]);
            }
        }
        frameCollection.data[frameCollection.actFrame].mi.setState(false);
        frameCollection.actFrame = frameCollection.nFrames;
        actFrame = frameCollection.actFrame;
        frameCollection.data[frameCollection.actFrame].mi.setState(true);

        ci.setImageData(frameDst);
        
        setTitleImageFilter();
        
        ColorImageScrollPaneProperties prop = new ColorImageScrollPaneProperties();
        prop.fitToView(true);
        frameCollection.setViewProperties(frameCollection.actFrame, prop);

        scrollpane.repaint();
        // ci.paint();
//        setSizeAndShow(dstSizeX, dstSizeY);
        cb_isSelected.setState(false);
        frameCollection.nFrames++;
        spectrumFrame.update();
        messageFrame.openTimed("Image filtered.");
      }
      else if (loadedClassInstance == null && frameCollection.nFrames <= 0)
        messageFrame.openTimed("No image loaded.\nNo filter loaded.");
      else if (loadedClassInstance == null)
        messageFrame.openTimed("No filter loaded.");
      else if (frameCollection.nFrames <= 0)
        messageFrame.openTimed("No image loaded.");

    }
    catch (NoSuchMethodException ex)
    {
      messageFrame.openTimed("Method filter()\nnot implemented.");
    }
    catch (Exception ex)
    {

      if (ex.getMessage() != null)
      {
        if (ex.getMessage().equals("SourceFrame2Exception"))
          messageFrame.openTimed("Source Frame 2 not selected.");
        else if (ex.getCause() != null)
          ex.getCause().printStackTrace();
        else
          ex.printStackTrace();
      }
      else if (ex.getCause() != null)
        ex.getCause().printStackTrace();
      else
        ex.printStackTrace();
    }
  }

  public void mergeImage()
  {
    try
    {
      boolean cb0 = mergeFrame.cb_featureImage0.getState();
      boolean cb1 = mergeFrame.cb_featureImage1.getState();

      int index0 = Utility.str2int(mergeFrame.tf_baseImage.getText());
      int index1 = index0, index2 = index0;

      if (cb0)
        index1 = Utility.str2int(mergeFrame.tf_featureImage0.getText());
      if (cb1)
        index2 = Utility.str2int(mergeFrame.tf_featureImage1.getText());

      int sizeX = frameCollection.data[index0].sizeX;
      int sizeY = frameCollection.data[index0].sizeY;

      if (index0 < 0 || index1 < 0 || index2 < 0
          || index0 >= frameCollection.nFrames
          || index1 >= frameCollection.nFrames
          || index2 >= frameCollection.nFrames)
      {
        messageFrame.openTimed("Incorrect index or size.");
      }
      else if (sizeX != frameCollection.data[index1].sizeX
          || sizeX != frameCollection.data[index2].sizeX
          || sizeY != frameCollection.data[index1].sizeY
          || sizeY != frameCollection.data[index2].sizeY)
      {
        messageFrame.openTimed("Incorrect index or size.");
      }
      else
      {
        int actFrame; //= frameCollection.actFrame;
        // int sizeX = frameCollection.data[index0].sizeX;
        // int sizeY = frameCollection.data[index0].sizeY;
        String name = frameCollection.data[index0].name;

        frameCollection.data[frameCollection.nFrames] = new FrameBuffer(sizeX,
            sizeY, name + "-F", false);

        m_image.add(frameCollection.data[frameCollection.nFrames].mi);
        frameCollection.data[frameCollection.nFrames].mi
            .addItemListener((ItemListener) this);

        byte[][][] frameDstByte = frameCollection.data[frameCollection.nFrames].data;
        byte[][][] frameSrc0Byte = frameCollection.data[index0].data;
        byte[][][] frameSrc1Byte = frameSrc0Byte, frameSrc2Byte = frameSrc0Byte;

        if (cb0)
          frameSrc1Byte = frameCollection.data[index1].data;
        if (cb1)
          frameSrc2Byte = frameCollection.data[index2].data;

        double[][][] frameDst = new double[3][sizeX][sizeY];
        double[][][] frameSrc0 = new double[3][sizeX][sizeY];
        double[][][] frameSrc1 = new double[3][sizeX][sizeY];
        double[][][] frameSrc2 = new double[3][sizeX][sizeY];

        for (int b = 0; b < 3; b++)
        {
          for (int x = 0; x < sizeX; x++)
            for (int y = 0; y < sizeY; y++)
            {
              frameSrc0[b][x][y] = Utility.byte2double(frameSrc0Byte[b][x][y]);
              if (cb0)
                frameSrc1[b][x][y] = Utility
                    .byte2double(frameSrc1Byte[b][x][y]);
              if (cb1)
                frameSrc2[b][x][y] = Utility
                    .byte2double(frameSrc2Byte[b][x][y]);
            }
        }

        double[] src0 = new double[3], src1 = new double[3], src2 = new double[3];

        int c0 = mergeFrame.c_featureImage0.getSelectedIndex();
        int c1 = mergeFrame.c_featureImage1.getSelectedIndex();

        for (int x = 0; x < sizeX; x++)
          for (int y = 0; y < sizeY; y++)
          {
            for (int b = 0; b < 3; b++)
            {
              src0[b] = frameSrc0[b][x][y];
              if (cb0)
                src1[b] = frameSrc1[b][x][y];
              else
                src1[b] = 0.0;
              if (cb1)
                src2[b] = frameSrc2[b][x][y];
              else
                src2[b] = 0.0;
            }

            if (src1[0] == 0.0 && src1[1] == 0.0 && src1[2] == 0.0
                && src2[0] == 0.0 && src2[1] == 0.0 && src2[2] == 0.0)
            {
              for (int b = 0; b < 3; b++)
                frameDst[b][x][y] = src0[b];
            }
            else
            {
              if (c0 == 1)
              {
                src1[0] = Math.max(Math.max(src1[0], src1[1]), src1[2]);
                src1[1] = 0.0;
                src1[2] = 0.0;
              }
              else if (c0 == 2)
              {
                src1[1] = Math.max(Math.max(src1[0], src1[1]), src1[2]);
                src1[0] = 0.0;
                src1[2] = 0.0;
              }
              else if (c0 == 3)
              {
                src1[2] = Math.max(Math.max(src1[0], src1[1]), src1[2]);
                src1[0] = 0.0;
                src1[1] = 0.0;
              }

              if (c1 == 1)
              {
                src2[0] = Math.max(Math.max(src2[0], src2[1]), src2[2]);
                src2[1] = 0.0;
                src2[2] = 0.0;
              }
              else if (c1 == 2)
              {
                src2[1] = Math.max(Math.max(src2[0], src2[1]), src2[2]);
                src2[0] = 0.0;
                src2[2] = 0.0;
              }
              else if (c1 == 3)
              {
                src2[2] = Math.max(Math.max(src2[0], src2[1]), src2[2]);
                src2[0] = 0.0;
                src2[1] = 0.0;
              }

              for (int b = 0; b < 3; b++)
                frameDst[b][x][y] = src1[b] + src2[b];
            }
          }

        for (int b = 0; b < 3; b++)
        {
          for (int x = 0; x < sizeX; x++)
            for (int y = 0; y < sizeY; y++)
            {
              frameDstByte[b][x][y] = Utility.double2byte(frameDst[b][x][y]);
            }
        }

        frameCollection.data[frameCollection.actFrame].mi.setState(false);
        frameCollection.actFrame = frameCollection.nFrames;
//        actFrame = frameCollection.actFrame;
        frameCollection.data[frameCollection.actFrame].mi.setState(true);
        int[][] ih = new int[3][sizeX * sizeY];

        for (int b = 0; b < 3; b++)
        {
          for (int x = 0; x < sizeX; x++)
            for (int y = 0; y < sizeY; y++)
            {
              ih[b][x + y * sizeX] = Utility.byte2int(frameDstByte[b][x][y]);
            }
        }

        ci.wr.setSamples(0, 0, sizeX, sizeY, 0, ih[0]);
        ci.wr.setSamples(0, 0, sizeX, sizeY, 1, ih[1]);
        ci.wr.setSamples(0, 0, sizeX, sizeY, 2, ih[2]);

        setTitleImageFilter();
        // ci.paint();
        ColorImageScrollPaneProperties prop = new ColorImageScrollPaneProperties();
        prop.fitToView(true);
        frameCollection.setViewProperties(frameCollection.actFrame, prop);

        scrollpane.setProperties(prop);
        frameCollection.nFrames++;
        spectrumFrame.update();
        messageFrame.openTimed("Image merged.");
      }
    }
    catch (Exception ex)
    {
      messageFrame.openTimed("Incorrect index or size.");
      // ex.printStackTrace();
    }
  }

  public void showImage()
  {
    try
    {
      if (frameCollection.nFrames > 0)
      {
        new SubFrame(this, frameCollection.data[frameCollection.actFrame].name);
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public void filterProperties()
  {
    try
    {
      if (loadedClassInstance instanceof AbstractFilter)
      {
        AbstractFilter filter = (AbstractFilter) loadedClassInstance;

        if (filter.hasProperties())
        {
          Point p = getLocation();
          Frame propertiesFrame = filter.getPropertiesFrame();
          propertiesFrame.setLocation(p.x + getWidth(), p.y);
          propertiesFrame.setVisible(true);
        }
        else
        {
          messageFrame.openTimed("Filter properties\nnot supported.");
        }
      }
      else if (loadedClassInstance != null)
      {
        try
        {
          Point p = getLocation();
          p.translate(50, 50);

          Class[] classType = new Class[1];
          classType[0] = p.getClass();

          Object[] parameter = new Object[1];
          parameter[0] = p;

          Class clss = loadedClassInstance.getClass();
          Method method = clss.getMethod("setPropertiesLocation", classType);
          method.invoke(loadedClassInstance, parameter);
        }
        catch (Exception ex)
        {
          // ex.printStackTrace();
        }

        final Class[] emptyC = {};
        final Object[] empty = {};

        Class clss = loadedClassInstance.getClass();
        Method method = clss.getMethod("openProperties", emptyC);
        method.invoke(loadedClassInstance, empty);
      }
      else
        messageFrame.openTimed("No filter loaded.");
    }
    catch (Exception ex)
    {
      messageFrame.openTimed("Filter properties\nnot supported.");
      // ex.printStackTrace();
    }
  }

  public void openSpectrum()
  {
    try
    {
      if (frameCollection.nFrames > 0)
      {
        spectrumFrame.open();
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public void openMerge()
  {
    mergeFrame.open();
  }

  public void openInfo()
  {
    messageFrame.open(Const.applicationName, 300, 200);
  }

  public void setImage(int i)
  {
    frameCollection.set(i);
  }

  public void setFilter(int i)
  {
    filterCollection.set(i);
  }

  public void deleteFilter()
  {
    filterCollection.delete();
  }

  public void Exit()
  {
    writeConfig();
    frameCollection.write();
    System.exit(0);
  }

  public void startThread(int m)
  {
    method = m;
    Thread t = new Thread(this);
    t.setPriority(Thread.MIN_PRIORITY);
    t.start();
  }

  public void setSizeLocationAndShow(int sizeX, int sizeY)
  {
    if (sizeX < Const.minSizeX)
      sizeX = Const.minSizeX;
    setLocation((Const.defaultScreenX - sizeX - 20) / 2, (Const.defaultScreenY
        - sizeY - 124) / 4);
    setSize(sizeX + 20, sizeY + 110);
    setVisible(true);
  }

  public void setSizeAndShow(int sizeX, int sizeY)
  {
    if (sizeX < Const.minSizeX)
      sizeX = Const.minSizeX;
    setSize(sizeX + 20, sizeY + 110);
    setVisible(true);
  }

  public void readConfig()
  {
    try
    {
      InputStream is = new FileInputStream(Const.configFilename);
      InputStreamReader isr = new InputStreamReader(is);
      BufferedReader br = new BufferedReader(isr);

      String str;
      str = br.readLine();
      str = br.readLine();
      str = br.readLine();
      str = br.readLine();
      str = br.readLine();

      str = br.readLine();
      int nFrames = Utility.str2int(str);
      str = br.readLine();
      str = br.readLine();

      str = br.readLine();
      int actFrame = Utility.str2int(str);
      str = br.readLine();
      str = br.readLine();

      str = br.readLine();
      int nFilters = Utility.str2int(str);
      str = br.readLine();
      str = br.readLine();

      str = br.readLine();
      int actFilter = Utility.str2int(str);
      str = br.readLine();
      str = br.readLine();

      for (int i = 0; i < nFilters; i++)
      {
        str = br.readLine();
        filterCollection.load(str);
      }

      if (nFrames > 0)
      {
        frameCollection.nFrames = nFrames;
        frameCollection.read();
        frameCollection.set(actFrame);
      }

      if (filterCollection.nFilters > 0
          && actFilter < filterCollection.nFilters)
        filterCollection.set2(actFilter);
      else if (filterCollection.nFilters > 0)
        filterCollection.set2(0);

      is.close();

      new File(Const.configFilename).delete();
    }
    catch (Exception ex)
    {
    }
  }

  public void writeConfig()
  {
    try
    {
      OutputStream os = new FileOutputStream(Const.configFilename);
      OutputStreamWriter osw = new OutputStreamWriter(os);
      PrintWriter pw = new PrintWriter(osw);

      pw.println("# This is a generated file. Do not modify.");
      pw.flush();

      pw.println("# Generated by: " + Const.className);
      pw.flush();

      pw.println("");
      pw.flush();

      pw.println("");
      pw.flush();

      pw.println("# Number of frames in frame collection:");
      pw.flush();

      pw.println(Utility.int2str(frameCollection.nFrames));
      pw.flush();

      pw.println("");
      pw.flush();

      pw.println("# Actual frame in frame collection:");
      pw.flush();

      pw.println(Utility.int2str(frameCollection.actFrame));
      pw.flush();

      pw.println("");
      pw.flush();

      pw.println("# Number of filters in filter collection:");
      pw.flush();

      pw.println(Utility.int2str(filterCollection.nFilters));
      pw.flush();

      pw.println("");
      pw.flush();

      pw.println("# Actual filter in filter collection:");
      pw.flush();

      pw.println(Utility.int2str(filterCollection.actFilter));
      pw.flush();

      pw.println("");
      pw.flush();

      pw.println("# Names of the filter classes (without .class postfix):");
      pw.flush();

      for (int i = 0; i < filterCollection.nFilters; i++)
      {
        pw.println(filterCollection.data[i].filename);
        pw.flush();
      }

      os.close();
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  public String readLine(InputStream is) throws Exception
  {
    String str = "";
    byte[] b = new byte[1];
    boolean go = true;

    while (go && (is.read(b) != -1))
    {
      if (b[0] != 10)
        str += new String(b);
      else
        go = false;
    }

    return str;
  }

  public void loadPpm(String filename, String name) throws Exception
  {
    String str, strX, strY;
    InputStream is = new FileInputStream(filename);

    while ((str = readLine(is)).startsWith("#") || str.startsWith("P6"))
      ;

    strX = str.substring(0, str.indexOf(" "));
    strY = str.substring(str.indexOf(" ") + 1);
    readLine(is);

    int sizeX = Utility.str2int(strX);
    int sizeY = Utility.str2int(strY);

    byte[] b = new byte[sizeX * sizeY * 3];

    is.read(b);
    is.close();

    int[] i = new int[sizeX * sizeY * 3];

    for (int k = 0; k < sizeX * sizeY * 3; k++)
      i[k] = Utility.byte2int(b[k]);

    ci.init(sizeX, sizeY);
    ci.wr.setPixels(0, 0, sizeX, sizeY, i);

    frameCollection.insert(sizeX, sizeY, name);
    ColorImageScrollPaneProperties prop = new ColorImageScrollPaneProperties();
    prop.fitToView(true);
    frameCollection.setViewProperties(frameCollection.actFrame, prop);

    scrollpane.setProperties(prop);
    // ci.paint();

    setTitleImageFilter();
    // setSizeAndShow(sizeX, sizeY);
  }

  public void loadImageFile(String filename, String name) throws Exception
  {
    BufferedImage bi = ImageIO.read(new File(filename));
    int sizeX = bi.getWidth();
    int sizeY = bi.getHeight();

    ci.init(bi.getWidth(), bi.getHeight());
    ci.setImage(bi);

    frameCollection.insert(sizeX, sizeY, name);
    ColorImageScrollPaneProperties prop = new ColorImageScrollPaneProperties();
    prop.fitToView(true);
    frameCollection.setViewProperties(frameCollection.actFrame, prop);

    scrollpane.setProperties(prop);
    // ci.paint();

    setTitleImageFilter();
  }

  public void cropImage()
  {
    if (scrollpane.getMode() == ColorImageScrollPane.MODE_CROP)
      scrollpane.setMode(ColorImageScrollPane.MODE_ZOOM);
    else
      scrollpane.setMode(ColorImageScrollPane.MODE_CROP);
  }

  public void savePpm(String name) throws Exception
  {
    int sizeX = ci.sizeX;
    int sizeY = ci.sizeY;

    OutputStream os = new FileOutputStream(name);

    os.write(("P6\n").getBytes());
    os.write(("# Generated by " + Const.className + "\n").getBytes());
    os.write((Utility.int2str(sizeX) + " " + Utility.int2str(sizeY) + "\n")
        .getBytes());
    os.write(("255\n").getBytes());

    byte[] b = new byte[sizeX * sizeY * 3];
    int[] i = new int[sizeX * sizeY * 3];

    ci.wr.getPixels(0, 0, sizeX, sizeY, i);

    for (int k = 0; k < sizeX * sizeY * 3; k++)
      b[k] = Utility.int2byte(i[k]);
    os.write(b);
    os.flush();
    os.close();
  }

  public void saveImageFile(String filename) throws Exception
  {
    int idx = filename.indexOf(".");
    String suffix = filename.substring(idx + 1, filename.length());
    ImageIO.write(ci.bi, suffix, new File(filename));
  }

  public void originalSize()
  {
    scrollpane.resetToOriginalSize();
  }

  public void fitToScreen()
  {
    scrollpane.fitToScreen();
  }

  class PrivateWindowListener extends WindowAdapter
  {
    PrivateWindowListener()
    {
      super();
    }

    public void windowClosing(WindowEvent ev)
    {
      try
      {
        Exit();
      }
      catch (Exception ex)
      {
        ex.printStackTrace();
      }
    }
  }

  public void mouseClicked(MouseEvent e)
  {
    if (e.getClickCount() > 1)
    {
      if (e.getSource() == scrollpane)
      {
    	if (scrollpane.getMode() == ColorImageScrollPane.MODE_CROP)
        {
          Rectangle cropSelection = scrollpane.getCropSelection();
          if (cropSelection == null)
          {
            messageFrame.openTimed("Image not croped.");
            scrollpane.setMode(ColorImageScrollPane.MODE_ZOOM);
            mi_cropImage.setState(false);

            return;
          }

          int actFrame = frameCollection.actFrame;
          int sizeX = frameCollection.data[actFrame].sizeX;
          int sizeY = frameCollection.data[actFrame].sizeY;
          String name = frameCollection.data[actFrame].name;

          int dstX = cropSelection.x;
          int dstY = cropSelection.y;
          int dstSizeX = cropSelection.width;
          int dstSizeY = cropSelection.height;
          if (dstX + dstSizeX >= sizeX)
            dstSizeX = sizeX - dstX;
          if (dstY + dstSizeY >= sizeY)
            dstSizeY = sizeY - dstY;

          frameCollection.data[frameCollection.nFrames] = new FrameBuffer(
              dstSizeX, dstSizeY, name + "-C", false);

          byte[][][] frameDstByte = frameCollection.data[frameCollection.nFrames].data;
          byte[][][] frameSrcByte = frameCollection.data[frameCollection.actFrame].data;

          for (int b = 0; b < 3; b++)
            for (int i = 0; i < dstSizeX; i++)
              for (int j = 0; j < dstSizeY; j++)
                frameDstByte[b][i][j] = frameSrcByte[b][i + dstX][j + dstY];

          frameCollection.data[frameCollection.actFrame].mi.setState(false);
          frameCollection.actFrame = frameCollection.nFrames;
          actFrame = frameCollection.actFrame;
          frameCollection.data[frameCollection.actFrame].mi.setState(true);
          ci.setImageData(frameDstByte);

          setTitleImageFilter();
          // ci.paint();
          ColorImageScrollPaneProperties prop = new ColorImageScrollPaneProperties();
          prop.fitToView(true);
          frameCollection.setViewProperties(frameCollection.actFrame, prop);

          scrollpane.setMode(ColorImageScrollPane.MODE_ZOOM);
          scrollpane.setProperties(prop);
          // setSizeAndShow(dstSizeX, dstSizeY);
          cb_isSelected.setState(false);
          frameCollection.nFrames++;
          spectrumFrame.update();
          mi_cropImage.setState(false);
          messageFrame.openTimed("Image croped.");
        } else if (loadedClassInstance instanceof AbstractFilter){
        	AbstractFilter filter = (AbstractFilter) loadedClassInstance;
    		Point p = scrollpane.getMousePosInsideImage(e.getPoint());
    		filter.handleMouseClick(p);
        }
      }
    }
  }

  public void mouseEntered(MouseEvent arg0)
  {
    // TODO Auto-generated method stub

  }

  public void mouseExited(MouseEvent arg0)
  {
    // TODO Auto-generated method stub

  }

  public void mousePressed(MouseEvent arg0)
  {
    // TODO Auto-generated method stub

  }

  public void mouseReleased(MouseEvent arg0)
  {
    // TODO Auto-generated method stub

  }
}
