package imageToolBox;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Choice;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.Panel;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class SpectrumFrame extends Frame implements ActionListener,
    ItemListener
{
  Button b_close;
  Choice c_band;
  Choice c_scale;
  Choice c_mode;
  MenuItem mi_properties;
  ImageToolBox iu;
  SpectrumApplet sa;
  SpectrumPropertiesFrame spf;

  byte[][][] frame;
  int sizeX;
  int sizeY;
  long[] max;
  long[][] spectrum;

  SpectrumFrame(ImageToolBox i)
  {
    iu = i;
    init();
  }

  public void init()
  {
    addWindowListener(new PrivateWindowListener());

    Panel p_north = new Panel();
    Panel p_south = new Panel();

    // ****************

    setLayout(new BorderLayout());
    p_north.setLayout(new FlowLayout());
    p_south.setLayout(new FlowLayout());

    // ****************

    Font f_title = new Font("Serif", Font.BOLD, 16);
    Label l_title = new Label("Spectrum");
    l_title.setFont(f_title);

    b_close = new Button("Close");
    b_close.addActionListener((ActionListener) this);

    c_band = new Choice();
    c_band.addItemListener((ItemListener) this);
    c_band.add("Red");
    c_band.add("Green");
    c_band.add("Blue");
    c_band.add("Gray");
    c_band.select("Gray");

    c_scale = new Choice();
    c_scale.addItemListener((ItemListener) this);
    c_scale.add("Linear");
    c_scale.add("Square");
    c_scale.select("Linear");

    c_mode = new Choice();
    c_mode.addItemListener((ItemListener) this);
    c_mode.add("Normal");
    c_mode.add("Smooth");
    c_mode.select("Normal");

    MenuBar mb = new MenuBar();
    Menu m_smooth = new Menu("Smooth");
    mi_properties = new MenuItem("Properties");
    mi_properties.addActionListener((ActionListener) this);

    mb.add(m_smooth);
    m_smooth.add(mi_properties);
    setMenuBar(mb);

    sa = new SpectrumApplet(this);
    spf = new SpectrumPropertiesFrame(this);

    // ****************

    p_north.add(l_title);
    p_south.add(c_band);
    p_south.add(c_scale);
    p_south.add(c_mode);
    p_south.add(b_close);

    // ****************

    add(p_north, "North");
    add(sa, "Center");
    add(p_south, "South");

    spectrum = new long[4][256];
    max = new long[4];
  }

  public void generateSpectrum()
  {
    frame = iu.frameCollection.data[iu.frameCollection.actFrame].data;
    sizeX = iu.frameCollection.data[iu.frameCollection.actFrame].sizeX;
    sizeY = iu.frameCollection.data[iu.frameCollection.actFrame].sizeY;

    for (int b = 0; b < 4; b++)
      for (int i = 0; i < 256; i++)
        spectrum[b][i] = 0;

    int red, green, blue, gray;
    for (int x = 0; x < sizeX; x++)
      for (int y = 0; y < sizeY; y++)
      {
        red = Utility.byte2int(frame[0][x][y]);
        green = Utility.byte2int(frame[1][x][y]);
        blue = Utility.byte2int(frame[2][x][y]);
        gray = (Utility.byte2int(frame[0][x][y])
            + Utility.byte2int(frame[1][x][y]) + Utility
            .byte2int(frame[2][x][y])) / 3;

        spectrum[0][red]++;
        spectrum[1][green]++;
        spectrum[2][blue]++;
        spectrum[3][gray]++;
      }

    for (int b = 0; b < 4; b++)
    {
      if (c_scale.getSelectedItem().equals("Square"))
      {
        for (int i = 1; i < 256; i++)
        {
          spectrum[b][i] *= (i * i);
        }
      }

      if (c_mode.getSelectedItem().equals("Smooth"))
      {
        int times = Utility.str2int(spf.tf_times.getText());
        double[] filter = new double[3];
        for (int f = 0; f < 3; f++)
          filter[f] = Utility.str2double(spf.tf_filter[f].getText());

        for (int k = 0; k < times; k++)
        {
          spectrum[b][0] = (long) ((filter[1] * spectrum[b][0] + filter[2]
              * spectrum[b][1]) / 3.0);

          for (int i = 1; i < 255; i++)
          {
            spectrum[b][i] = (long) ((filter[0] * spectrum[b][i - 1]
                + filter[1] * spectrum[b][i] + filter[2] * spectrum[b][i + 1]) / 3.0);
          }

          spectrum[b][255] = (long) ((filter[0] * spectrum[b][254] + filter[1]
              * spectrum[b][255]) / 3.0);
        }
      }

      max[b] = 0;

      for (int i = 0; i < 256; i++)
      {
        if (spectrum[b][i] > max[b])
          max[b] = spectrum[b][i];
      }

      for (int i = 0; i < 256; i++)
      {
        spectrum[b][i] *= 255;
        spectrum[b][i] /= max[b];
      }
    }
  }

  public void update()
  {
    if (isShowing())
    {
      generateSpectrum();
      sa.paint();
    }
  }

  public void itemStateChanged(ItemEvent e)
  {
    update();
  }

  public void open()
  {
    generateSpectrum();

    setBackground(new Color(236, 233, 216));
    setTitle("Spectrum");
    setSize(300, 420);
    Point p = iu.getLocation();
    p.translate(50, 50);
    setLocation(p);
    setVisible(true);
  }

  public void close()
  {
    setVisible(false);
  }

  public void actionPerformed(ActionEvent e)
  {
    try
    {
      Object obj = e.getSource();

      if (obj instanceof Button)
      {
        if (obj == b_close)
          close();
      }
      else if (obj instanceof MenuItem)
      {
        if (obj == mi_properties)
          spf.open();
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  class PrivateWindowListener extends WindowAdapter
  {
    PrivateWindowListener()
    {
      super();
    }

    public void windowClosing(WindowEvent ev)
    {
      close();
    }
  }
}
