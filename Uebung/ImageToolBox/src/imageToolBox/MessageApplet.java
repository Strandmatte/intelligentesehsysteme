package imageToolBox;

import java.applet.Applet;
import java.awt.Graphics;

public class MessageApplet extends Applet
{
  int xOff = 20;
  int yOff = 20;
  int dif = 15;
  int line = 0;

  String text;

  MessageApplet()
  {
    super();
  }

  public void print(Graphics g)
  {
    String head, tail = new String(text);

    line = 0;

    while (!tail.equals(""))
    {
      if (tail.indexOf("\n") != -1)
      {
        head = tail.substring(0, tail.indexOf("\n"));
        tail = tail.substring(tail.indexOf("\n") + 1);
      }
      else
      {
        head = new String(tail);
        tail = new String("");
      }

      if (!head.equals(""))
        g.drawString(head, xOff, yOff + line * dif);
      line++;
    }
  }

  public void paint(Graphics g)
  {
    super.paint(g);
    print(g);
  }
}
