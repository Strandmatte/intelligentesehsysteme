package imageToolBox;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Point;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class MergeFrame extends Frame implements ActionListener
{
  ImageToolBox iu;

  Button b_close;
  Button b_merge;
  TextField tf_baseImage;
  TextField tf_featureImage0;
  TextField tf_featureImage1;
  Checkbox cb_featureImage0;
  Checkbox cb_featureImage1;
  Choice c_featureImage0;
  Choice c_featureImage1;

  public MergeFrame(ImageToolBox i)
  {
    super();
    iu = i;
    init();
  }

  public void init()
  {
    addWindowListener(new PrivateWindowListener());

    Panel p_north = new Panel();
    Panel p_center = new Panel();
    Panel p_south = new Panel();

    // ****************

    setLayout(new BorderLayout());
    p_north.setLayout(new FlowLayout());
    p_center.setLayout(new FlowLayout());
    p_south.setLayout(new FlowLayout());

    // ****************

    Font f_title = new Font("Serif", Font.BOLD, 16);
    Label l_title = new Label("Merge");
    l_title.setFont(f_title);

    Label l_baseImage = new Label("Base Image");

    b_close = new Button("Close");
    b_close.addActionListener((ActionListener) this);

    b_merge = new Button("Merge");
    b_merge.addActionListener((ActionListener) this);

    tf_baseImage = new TextField("0");
    tf_featureImage0 = new TextField("0");
    tf_featureImage1 = new TextField("0");

    cb_featureImage0 = new Checkbox("Feature Image");
    cb_featureImage1 = new Checkbox("Feature Image");

    c_featureImage0 = new Choice();
    c_featureImage0.add("Original");
    c_featureImage0.add("Red");
    c_featureImage0.add("Green");
    c_featureImage0.add("Blue");

    c_featureImage1 = new Choice();
    c_featureImage1.add("Original");
    c_featureImage1.add("Red");
    c_featureImage1.add("Green");
    c_featureImage1.add("Blue");

    // ****************

    add(p_north, "North");
    add(p_center, "Center");
    add(p_south, "South");

    p_north.add(l_title);

    p_center.add(l_baseImage);
    p_center.add(tf_baseImage);

    p_center.add(cb_featureImage0);
    p_center.add(tf_featureImage0);
    p_center.add(c_featureImage0);

    p_center.add(cb_featureImage1);
    p_center.add(tf_featureImage1);
    p_center.add(c_featureImage1);

    p_south.add(b_merge);
    p_south.add(b_close);
  }

  public void open()
  {
    setBackground(new Color(236, 233, 216));
    setTitle("Merge");
    setSize(230, 200);
    Point p = iu.getLocation();
    p.translate(50, 50);
    setLocation(p);
    setVisible(true);
  }

  public void close()
  {
    setVisible(false);
  }

  public void actionPerformed(ActionEvent e)
  {
    try
    {
      Object obj = e.getSource();

      if (obj instanceof Button)
      {
        if (obj == b_close)
          close();
        if (obj == b_merge)
          iu.mergeImage();
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }

  class PrivateWindowListener extends WindowAdapter
  {
    PrivateWindowListener()
    {
      super();
    }

    public void windowClosing(WindowEvent ev)
    {
      close();
    }
  }
}
