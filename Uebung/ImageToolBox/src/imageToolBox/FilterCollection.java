package imageToolBox;
import java.awt.event.ItemListener;
import java.lang.reflect.Method;

public class FilterCollection
{
  int nFilters;
  int actFilter;
  Filter[] data;
  ImageToolBox iu;

  public FilterCollection(ImageToolBox i)
  {
    nFilters = 0;
    actFilter = -1;
    data = new Filter[1024];
    iu = i;
  }

  public void load(String filename)
  {
    try
    {
      Object instance = iu.classLoader.loadClass(filename).newInstance();
      if(instance instanceof AbstractFilter)
      {
        ((AbstractFilter)instance).initPropertiesFrame();
      }
      insert(instance, filename, filename);
    }
    catch (Exception ex)
    {
      // ex.printStackTrace();
    }
  }

  public int loaded(String filename)
  {
    int index = -1;
    for (int i = 0; i < nFilters; i++)
      if (data[i].filename.equals(filename))
        index = i;
    return index;
  }

  public void insert(Object i, String f, String n)
  {
    data[nFilters] = new Filter(i, f, n);

    if (nFilters == 0)
      iu.m_filter.add(iu.mi_filterSeparator);
    iu.m_filter.add(data[nFilters].mi);
    data[nFilters].mi.addItemListener((ItemListener) iu);

    if (actFilter >= 0)
      data[actFilter].mi.setState(false);
    actFilter = nFilters;
    data[actFilter].mi.setState(true);
    nFilters++;

    iu.setTitleImageFilter();
  }

  public void delete()
  {
    if (nFilters > 0)
    {
      try
      {
        final Class[] emptyC = {};
        final Object[] empty = {};

        Class clss = iu.loadedClassInstance.getClass();
        Method method = clss.getMethod("closeProperties", emptyC);
        method.invoke(iu.loadedClassInstance, empty);
      }
      catch (Exception ex)
      {
      }

      iu.m_filter.remove(data[actFilter].mi);
      if (nFilters == 1)
        iu.m_filter.remove(iu.mi_filterSeparator);

      for (int i = actFilter; i < nFilters - 1; i++)
        data[i] = data[i + 1];
      nFilters--;
      data[nFilters] = null;
      if (actFilter == nFilters)
        actFilter--;

      if (nFilters > 0)
      {
        iu.loadedClassInstance = data[actFilter].instance;
        data[actFilter].mi.setState(true);
      }
      else
        iu.loadedClassInstance = null;

      iu.setTitleImageFilter();
      // iu.messageFrame.openTimed("Filter deleted.");
    }
  }

  public void set(int i)
  {
    if (i != actFilter)
    {
      if (actFilter >= 0)
        data[actFilter].mi.setState(false);
      actFilter = i;
      iu.loadedClassInstance = data[actFilter].instance;
      iu.setTitleImageFilter();
      // iu.messageFrame.openTimed("Switching to filter.");
    }
    else
    {
      data[actFilter].mi.setState(true);
      iu.loadedClassInstance = data[actFilter].instance;
    }
  }

  public void set2(int i)
  {
    if (actFilter >= 0)
      data[actFilter].mi.setState(false);
    actFilter = i;
    data[actFilter].mi.setState(true);
    iu.loadedClassInstance = data[i].instance;
    iu.setTitleImageFilter();
  }
}
