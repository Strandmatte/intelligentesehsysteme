/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class Gau�_TU_MD extends ConvolutionFilter_TU_MD {

	double sigma = 1.4;
	int dimension = 5;
	double summe = 0;

	@Override
	public double[][] getKernel() {
		double[][] kernel = new double[dimension][dimension];
		//f�llen der Maske nach Formel aus Vorlesung
		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
				kernel[i][j] = (1/(2*Math.PI*Math.pow(sigma, 2))) * Math.exp(-(Math.pow(i-2, 2)+Math.pow(j-2, 2))/(2*Math.pow(sigma, 2)));
				summe = kernel[i][j] + summe;
			}
		}
		//normalisieren
		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
				kernel[i][j] = kernel[i][j] / summe;
			}
		}
		
		summe = 0;
		
		return kernel;
	}
}
