import imageToolBox.AbstractFilter;

import java.awt.Point;


public class SelectionExample extends AbstractFilter {
	
	private Point p;
	
	@Override
	public void handleMouseClick(Point p) {
		System.out.println(p);
		this.p = p;
	}
	
	@Override
	public void filter(double[][][] src, double[][][] dst) {
		for (int x=0; x<width; x++) {
			for (int y=0; y<height; y++) {
				if (Math.abs(x - p.x) <= 1 && Math.abs(y - p.y) <=1) {
					dst[0][x][y] = 255;
				} else {
					for (int k=0; k<3; k++) {
						dst[k][x][y] = src[k][x][y];
					}
				}
			}
		}
	}

}
