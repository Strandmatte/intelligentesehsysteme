import imageToolBox.AbstractFilter;

/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class KontrastaufloesungVonGraubilder_MD_TU extends AbstractFilter {
	@Override
	public void filter(double[][][] src, double[][][] dst) {

		double teiler = getDoubleProperty("teiler"); //z.B. 3
		int abschnitt = (int)(255/ teiler);	//f�r 3 dann ~85
		int grauwerte = (int)(255/(teiler-1));	//bei 3 dann 128

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				int mittelwert = (int)((src[0][i][j] + src[1][i][j] + src[2][i][j]) / 3);
				int faktor = (int) (mittelwert / abschnitt); //nimmt Grauwert des Bildes und vergleicht mit Abschnitt, z.B. 23<85 also 0
				int wert = faktor*grauwerte;
				for (int k = 0; k < 3; k++) {					
					dst[k][i][j] = wert; //errechnet auf welchen Wert abgebildet wird
				}
			}
		}
	}

	@Override
	public boolean hasProperties() {
		return true;
	}

	@Override
	public void initGUI() {
		addDoubleProperty("teiler", 4);
	}
}
