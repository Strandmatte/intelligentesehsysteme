import imageToolBox.AbstractFilter;

import java.util.Arrays;

/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public abstract class ConvolutionFilter_TU_MD extends AbstractFilter {

	@Override
	public void filter(double[][][] src, double[][][] dst) {

		double[][] kernel = getKernel(); // gibt Maske
		int l�nge = kernel.length; // fragt Dimension der Maske ab
		int distanz = (l�nge / 2);// Zur Bestimmung der Pixel, deren Nachbarn
									// au�erhalb der Indizes liegen w�rden, z.B.
									// 5 muss bei 2 starten
		double[][] submatrix = new double[l�nge][l�nge]; // Zur Erstellung der
															// Submatrix auf die
															// Konv. angewendet
															// wird
		double summe;
		int index;

		for (int i = distanz; i < width - distanz; i++) {
			for (int j = distanz; j < height - distanz; j++) { // geht �ber alle
																// Pixel

				index = 0;

				for (int k = 0; k < l�nge; k++) {

					submatrix[index] = Arrays.copyOfRange(src[0][i - distanz
							+ k], j - distanz, j + distanz + 1); // f�llen der
																	// einzelnen
																	// Felder
					index++;
				}

				summe = 0;

				for (int l = 0; l < l�nge; l++) {
					for (int m = 0; m < l�nge; m++) {

						summe = summe + (submatrix[m][l] * kernel[l][m]); // Anwendung
																			// der
																			// Konvolution
					}

					dst[0][i][j] = summe;
					dst[1][i][j] = summe;
					dst[2][i][j] = summe;
				}
			}

		}
	}

	public abstract double[][] getKernel();
}