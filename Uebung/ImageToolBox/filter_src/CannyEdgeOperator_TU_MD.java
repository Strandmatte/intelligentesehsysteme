import java.util.Arrays;

import imageToolBox.AbstractFilter;

/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class CannyEdgeOperator_TU_MD extends AbstractFilter {
	// stage regelt Stufe des Alg. d.h. 0=nichts; 3=alles bis NMS ausf�hren etc
	double stage;
	double sigma;
	double t1;
	double t2;

	// Array zum Mitf�hren der Gradientenbetr�ge & -richtungen
	double[][][] gradients;

	@Override
	public void filter(double[][][] src, double[][][] dst) {

		stage = getDoubleProperty("stage");
		sigma = getDoubleProperty("sigma");
		t1 = getDoubleProperty("t1");
		t2 = getDoubleProperty("t2");

		if (stage >= 1) {
			gauss(src, dst);
			for (int k = 0; k < 3; k++) {
				for (int i = 0; i < width; i++) {
					for (int j = 0; j < height; j++) {
						src[k][i][j] = dst[k][i][j];
					}
				}
			}
		}
		if (stage >= 2) {
			sobel(src, dst);
		}
		if (stage >= 3) {
			nonMaxima(dst);
			for (int k = 0; k < 3; k++) {
				for (int i = 0; i < width; i++) {
					for (int j = 0; j < height; j++) {
						src[k][i][j] = dst[k][i][j];
					}
				}
			}
		}
		if (stage >= 4) {
			fusion(src,dst);
		}
	}

	public void gauss(double[][][] src, double[][][] dst) {
		int dimension = 5;
		double teiler = 0;

		double[][] kernel = new double[dimension][dimension];
		// f�llen der Maske nach Formel aus Vorlesung
		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
				kernel[i][j] = (1 / (2 * Math.PI * Math.pow(sigma, 2)))
						* Math.exp(-(Math.pow(i - 2, 2) + Math.pow(j - 2, 2))
								/ (2 * Math.pow(sigma, 2)));
				teiler = kernel[i][j] + teiler;
			}
		}
		// normalisieren
		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
				kernel[i][j] = kernel[i][j] / teiler;
			}
		}
		
		double summe = 0;
		int l�nge = kernel.length;
		int distanz = (l�nge / 2);
		double[][] submatrix = new double[l�nge][l�nge];
		int index;

		for (int i = distanz; i < width - distanz; i++) {
			for (int j = distanz; j < height - distanz; j++) {
				for (int p = 0; p < 3; p++) {
					index = 0;

					for (int k = 0; k < l�nge; k++) {
						submatrix[index] = Arrays.copyOfRange(src[p][i
								- distanz + k], j - distanz, j + distanz + 1);
						index++;
					}
					summe = 0;

					for (int l = 0; l < l�nge; l++) {
						for (int m = 0; m < l�nge; m++) {
							summe = summe + (submatrix[m][l] * kernel[l][m]);
						}
						dst[p][i][j] = summe;
					}
				}
			}
		}
	}

	public void sobel(double[][][] src, double[][][] dst) {
		gradients = new double[2][width][height];
		// Sobel Masken
		double[][] horizontal = { { -1.0, 0.0, 1.0 }, { -2.0, 0.0, 2.0 },
				{ -1.0, 0.0, 1.0 } };
		double[][] vertikal = { { 1.0, 2.0, 1.0 }, { 0.0, 0.0, 0.0 },
				{ -1.0, -2.0, -1.0 } };
		// f�r die sp�tere Grauwertspreizung
		double max = 255;
		double min = 255;
		int l�nge = 3;
		int distanz = (l�nge / 2);
		// f�r Gradient & Richtung
		double[][] submatrix = new double[l�nge][l�nge];
		int index;

		for (int i = distanz; i < width - distanz; i++) {
			for (int j = distanz; j < height - distanz; j++) {
				index = 0;
				for (int k = 0; k < l�nge; k++) {
					submatrix[index] = Arrays.copyOfRange(src[0][i - distanz
							+ k], j - distanz, j + distanz + 1);
					index++;
				}

				double summeVertikal = 0;
				double summeHorizontal = 0;
				for (int l = 0; l < l�nge; l++) {
					for (int m = 0; m < l�nge; m++) {
						// berechnet die Werte f�r einen Pixel je Maske
						summeVertikal = summeVertikal
								+ (submatrix[m][l] * vertikal[l][m]);
						summeHorizontal = summeHorizontal
								+ (submatrix[m][l] * horizontal[l][m]);
					}
					// Gradientenberechnung
					double gradient = (int) Math
							.sqrt(summeVertikal * summeVertikal
									+ summeHorizontal * summeHorizontal);
					// Winkelbestimmung
					int winkel = (int) Math.atan(summeVertikal
							/ summeHorizontal);
					winkel = (int) (winkel * 360 / Math.PI);

					if (winkel < 22.5 || (winkel >= 157.5 && winkel < 202.5)
							|| winkel >= 337.5) {
						winkel = 0;
					} else if ((winkel >= 22.5 && winkel < 67.5)
							|| (winkel >= 202.5 && winkel < 247.5)) {
						winkel = 45;
					} else if ((winkel >= 67.5 && winkel < 112.5)
							|| (winkel >= 247.5 && winkel < 292.5)) {
						winkel = 90;
					} else if ((winkel >= 112.5 && winkel < 157.5)
							|| (winkel >= 292.5 && winkel < 337.5)) {
						winkel = 135;
					}
					// Aktualisierung MaxGiven
					if (gradient > max)
						max = gradient;
					if (gradient < min)
						min = gradient;
					// Ausgabe aktualisieren
					dst[0][i][j] = gradient;
					dst[1][i][j] = gradient;
					dst[2][i][j] = gradient;
					// Mitf�hren St�rke & Richtung
					gradients[0][i][j] = gradient;
					gradients[1][i][j] = winkel;
				}
			}

		}
		// Grauwertspreizung
		for (int n = 0; n < width; n++) {
			for (int o = 0; o < height; o++) {
				for (int k = 0; k < 3; k++) {
					dst[k][n][o] = (dst[k][n][o] - min) * (255 / (max - min));
				}
				gradients[0][n][o] = (int) ((gradients[0][n][o] - min) * (255 / (max - min)));
			}
		}
	}

	public void nonMaxima(double[][][] dst) {

		int l�nge = 3;
		int distanz = (l�nge / 2);

		for (int i = distanz; i < width - distanz; i++) {
			for (int j = distanz; j < height - distanz; j++) {

				if (gradients[1][i][j] == 0) {
					if ((gradients[0][i - 1][j] > gradients[0][i][j] && gradients[1][i - 1][j] == 0)
							|| (gradients[0][i + 1][j] > gradients[0][i][j] && gradients[1][i + 1][j] == 0)) {
						dst[0][i][j] = 0;
						dst[1][i][j] = 0;
						dst[2][i][j] = 0;
					}
				} else if (gradients[1][i][j] == 135) {
					if ((gradients[0][i - 1][j - 1] > gradients[0][i][j] && gradients[1][i - 1][j - 1] == 135)
							|| (gradients[0][i + 1][j + 1] > gradients[0][i][j] && gradients[1][i + 1][j + 1] == 135)) {
						dst[0][i][j] = 0;
						dst[1][i][j] = 0;
						dst[2][i][j] = 0;
					}
				} else if (gradients[1][i][j] == 90) {
					if ((gradients[0][i][j - 1] > gradients[0][i][j] && gradients[1][i][j - 1] == 90)
							|| (gradients[0][i][j + 1] > gradients[0][i][j] && gradients[1][i][j + 1] == 90)) {
						dst[0][i][j] = 0;
						dst[1][i][j] = 0;
						dst[2][i][j] = 0;
					}
				} else if (gradients[1][i][j] == 45) {
					if ((gradients[0][i + 1][j - 1] > gradients[0][i][j] && gradients[1][i + 1][j - 1] == 45)
							|| (gradients[0][i - 1][j + 1] > gradients[0][i][j] && gradients[1][i - 1][j + 1] == 45)) {
						dst[0][i][j] = 0;
						dst[1][i][j] = 0;
						dst[2][i][j] = 0;
					}
				}
			}
		}
	}

	public void fusion(double[][][] src, double[][][] dst) {

		// naiver Ansatz �ber Nachtbarschaft und Vergleich G.Richtung
		double[][] submatrix = new double[3][3];

		// Geht �ber alle Pixel und ordnet schwach/mittel/stark zu
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				for (int k = 0; k < 3; k++) {
					if (src[k][i][j] < t2) {
						dst[0][i][j] = 0;
						dst[1][i][j] = 0;
						dst[2][i][j] = 0;
					} else if (src[k][i][j] < t1) {
						dst[0][i][j] = 255;
						dst[1][i][j] = 0;
						dst[2][i][j] = 0;
					} else if (src[k][i][j] >= t1) {
						dst[0][i][j] = 255;
						dst[1][i][j] = 255;
						dst[2][i][j] = 255;
					}
				}
			}
		}

		for (int i = 1; i < width - 1; i++) {
			for (int j = 1; j < height - 1; j++) {
				int index = 0;
				// wenn starke Kante
				if (dst[1][i][j] == 255) {
					// alle starken und schwachen Kanten suchen
					for (int k = 0; k < 3; k++) {
						submatrix[index] = Arrays.copyOfRange(
								dst[0][i - 1 + k], j - 1, j + 1 + 1);
						index++;
					}
					index = 0;
					loop: for (int l = 0; l < 3; l++) {
						int n = 0;
						for (int m = 0; m < 3; m++) {
							n = m;
							if (submatrix[l][m] == 255) {
								dst[0][i + l - 1][j + m - 1] = 255;
								dst[1][i + l - 1][j + m - 1] = 255;
								dst[2][i + l - 1][j + m - 1] = 255;
								break loop;
							}
						}
						dst[0][i + l - 1][j + n - 1] = 0;
						dst[1][i + l - 1][j + n - 1] = 0;
						dst[2][i + l - 1][j + n - 1] = 0;
					}
				}
			}
		}
		for (int i = 1; i < width - 1; i++) {
			for (int j = 1; j < height - 1; j++) {
				int index = 0;
				// wenn schwache Kante
				if (dst[0][i][j] == 255 && dst[1][i][j] != 255) {
					// alle starken Kanten suchen
					for (int k = 0; k < 3; k++) {
						submatrix[index] = Arrays.copyOfRange(
								dst[1][i - 1 + k], j - 1, j + 1 + 1);
						index++;
					}
					index = 0;
					loop: for (int l = 0; l < 3; l++) {
						for (int m = 0; m < 3; m++) {
							if (submatrix[l][m] == 255
									&& (gradients[1][i][j] == gradients[1][i
											+ l - 1][j + m - 1])) {
								dst[0][i][j] = 255;
								dst[1][i][j] = 255;
								dst[2][i][j] = 255;
								break loop;
							}
						}
						dst[0][i][j] = 0;
						dst[1][i][j] = 0;
						dst[2][i][j] = 0;
					}
				}
			}
		}
	}

	public boolean hasProperties() {
		return true;
	}

	public void initGUI() {
		addDoubleProperty("stage", 4.0);
		addDoubleProperty("sigma", 1.4);
		addDoubleProperty("t1", 13.0);
		addDoubleProperty("t2", 5.0);
	}
}
