import imageToolBox.AbstractFilter;

/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class InteraktiveBinärisierung_TU_MD extends AbstractFilter {
	@Override
	public void filter(double[][][] src, double[][][] dst) {

		//Schwellenwerte müssen aufsteigend eingegeben werden, da keine Sortierfunktion implementiert
		double tresh1 = getDoubleProperty("Schwellwert 1");
		double tresh2 = getDoubleProperty("Schwellwert 2");
		double tresh3 = getDoubleProperty("Schwellwert 3");
		double tresh4 = getDoubleProperty("Schwellwert 4");
		double tresh5 = getDoubleProperty("Schwellwert 5");
		double tresh6 = getDoubleProperty("Schwellwert 6");
		double tresh7 = getDoubleProperty("Schwellwert 7");
		double tresh8 = getDoubleProperty("Schwellwert 8");
		double tresh9 = getDoubleProperty("Schwellwert 9");

		//Geht über alle Pixel und ordnet den einzelnen Regionen zu
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				for (int k = 0; k < 3; k++) {
					if (src[k][i][j] < tresh1) {
						dst[0][i][j] = 0;
						dst[1][i][j] = 0;
						dst[2][i][j] = 0;
					} else if (src[k][i][j] < tresh2) {
						dst[0][i][j] = 255;
						dst[1][i][j] = 0;
						dst[2][i][j] = 0;
					} else if (src[k][i][j] < tresh3) {
						dst[0][i][j] = 0;
						dst[1][i][j] = 255;
						dst[2][i][j] = 0;
					} else if (src[k][i][j] < tresh4) {
						dst[0][i][j] = 0;
						dst[1][i][j] = 0;
						dst[2][i][j] = 255;
					} else if (src[k][i][j] < tresh5) {
						dst[0][i][j] = 255;
						dst[1][i][j] = 255;
						dst[2][i][j] = 0;
					} else if (src[k][i][j] < tresh6) {
						dst[0][i][j] = 255;
						dst[1][i][j] = 0;
						dst[2][i][j] = 255;
					} else if (src[k][i][j] < tresh7) {
						dst[0][i][j] = 0;
						dst[1][i][j] = 255;
						dst[2][i][j] = 255;
					} else if (src[k][i][j] < tresh8) {
						dst[0][i][j] = 138;
						dst[1][i][j] = 43;
						dst[2][i][j] = 226;
					} else if (src[k][i][j] < tresh9) {
						dst[0][i][j] = 255;
						dst[1][i][j] = 127;
						dst[2][i][j] = 0;
					} else if (src[k][i][j] >= tresh9){
						dst[0][i][j]=255;
						dst[1][i][j]=255;
						dst[2][i][j]=255;
						}
				}
			}
		}
	}

	public boolean hasProperties() {
		return true;
	}

	public void initGUI() {
		addDoubleProperty("Schwellwert 1", 0);
		addDoubleProperty("Schwellwert 2", 0);
		addDoubleProperty("Schwellwert 3", 0);
		addDoubleProperty("Schwellwert 4", 0);
		addDoubleProperty("Schwellwert 5", 0);
		addDoubleProperty("Schwellwert 6", 0);
		addDoubleProperty("Schwellwert 7", 0);
		addDoubleProperty("Schwellwert 8", 0);
		addDoubleProperty("Schwellwert 9", 0);
	}
}
