
public class ImageAdder
{
  public String getID()
  {
    return new String("<id>ImageToolBox</id>");
  }

  // Neue Methode:
  //
  // Wenn die Filter-Methode zwei Source Images verarbeitet,
  // dann muss folgende Methode implementiert werden.
  //
  public String getNSource()
  {
    return new String("<nsource>2</nsource>");
  }

  // Filter-Methode mit zwei Source Images:
  //
  // src0 = momentan sichtbares Image. (wie gehabt)
  // src1 = Image mit selektierter Checkbox.
  // (In der GUI von ImageToolBox befindet sich jetzt unterhalb
  // des Image eine Checkbox.)
  //
  // Hinweis: Die Gr��e des dst-Image entspricht der Gr��e
  // des src0-Image.
  //
  public void filter(double[][][] src0, double[][][] src1, double[][][] dst)
  {
    int sizeX = src0[0].length;
    int sizeY = src0[0][0].length;

    for (int x = 0; x < sizeX; x++)
      for (int y = 0; y < sizeY; y++)
      {
        for(int b = 0; b < 3; b++)
        {
          if(src1[b][x][y] >= src0[b][x][y])
            dst[b][x][y] = src1[b][x][y];
          else
            dst[b][x][y] = src0[b][x][y];
        }
      }
  }
  
  
}
