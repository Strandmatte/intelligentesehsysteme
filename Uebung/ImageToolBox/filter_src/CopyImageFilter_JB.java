import imageToolBox.AbstractFilter;

public class CopyImageFilter_JB extends AbstractFilter {
	@Override
	public void filter(double[][][] src, double[][][] dst) {
		for (int x = 0; x < width; x++){
			for (int y = 0; y < height; y++){
				for (int k = 0; k < 3; k++){
					dst[k][x][y] = src[k][x][y];
				}
			}
		}
	}
}
