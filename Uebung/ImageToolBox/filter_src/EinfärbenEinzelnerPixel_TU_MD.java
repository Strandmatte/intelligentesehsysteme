import imageToolBox.AbstractFilter;

/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class Einf�rbenEinzelnerPixel_TU_MD extends AbstractFilter {
	@Override
	public void filter(double[][][] src, double[][][] dst) {

		double breite = width / 4;
		double h�he = height / 4;

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				for (int k = 0; k < 3; k++) {

					dst[k][i][j] = src[k][i][j];

					if (k > 0
							&& (i == 0 || j == 0 || i == (width - 1) || j == (height - 1))) {
						dst[k][i][j] = 0;
						dst[0][i][j] = 255;
					}
					if (k < 2
							&& (i == breite - 1 || i == 2 * breite - 1 || i == 3 * breite - 1)) {
						dst[k][i][j] = 0;
						dst[2][i][j] = 255;
					}

					if (k < 2
							&& (j == h�he - 1 || j == 2 * h�he - 1 || j == 3 * h�he - 1)) {
						dst[k][i][j] = 0;
						dst[2][i][j] = 255;
					}
				}
			}
		}
	}

}
