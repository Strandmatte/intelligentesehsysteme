import imageToolBox.AbstractFilter;
/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class Medianfilter_TU_MD extends AbstractFilter {
	
	public void filter(double[][][] src, double[][][] dst) {
		double dim = getDoubleProperty("Dimension");
		int dimension = (int) dim;
		
		double[][] list = new double[3][dimension*dimension];
		
		for (int i = 0; i < width; i++){
			for (int j = 0; j < height; j++) {
				for(int t = 0; t < list[0].length; t++){
					list[0][t] = 10000;
					list[1][t] = 10000;
					list[2][t] = 10000;
				}
				for (int k = 0; k < list[0].length; k++) {
					int h = j+(k/dimension)-1;
					int w = i+(k%dimension)-1;
					
					if(h>=0 && w>=0 && h<height && w<width){						
						int z = 0;
						while(list[0][z] < src[0][w][h]){
							z++;
						}
						for(int u = list[0].length-1; u > z; u--){
							list[0][u] = list[0][u-1];
						}
						list[0][z] = src[0][w][h];
						
						z = 0;
						while(list[1][z] < src[1][w][h]){
							z++;
						}
						for(int u = list[1].length-1; u > z; u--){
							list[1][u] = list[1][u-1];
						}
						list[1][z] = src[1][w][h];
						
						z = 0;
						while(list[2][z] < src[2][w][h]){
							z++;
						}
						for(int u = list[2].length-1; u > z; u--){
							list[2][u] = list[2][u-1];
						}
						list[2][z] = src[2][w][h];
					}
					else{
						for(int z = list[0].length-1; z > 0; z--){
							list[0][z] = list[0][z-1];
							list[1][z] = list[1][z-1];
							list[2][z] = list[2][z-1];
						}
						list[0][0] = -1;
						list[1][0] = -1;
						list[2][0] = -1;
					}
				}	
				
				int l = list[0].length;
				int k = 0;
				while(list[0][k] == -1){
					l--;
					k++;
				}
				
				dst[0][i][j] = list[0][k+(l/2)];
				dst[1][i][j] = list[1][k+(l/2)];
				dst[2][i][j] = list[2][k+(l/2)];
			}
		}		
	}
	
	public boolean hasProperties() {
		return true;
	}

	public void initGUI() {
		addDoubleProperty("Dimension", 3);
	}

}