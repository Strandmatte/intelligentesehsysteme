import imageToolBox.AbstractFilter;

import java.util.Arrays;

/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class Sobelfilter_TU_MD extends AbstractFilter {

	@Override
	public void filter(double[][][] src, double[][][] dst) {

		double[][] horizontal = {
				{-1.0, 0.0, 1.0},
				{-2.0, 0.0, 2.0},
				{-1.0, 0.0, 1.0}
				};
		
		double[][]vertikal = {
				{ 1.0,  2.0,  1.0},
				{ 0.0,  0.0,  0.0},
				{-1.0, -2.0, -1.0}
				};
		
		
		double max = 255;
		
				
		int l�nge = 3; 
		int distanz = (l�nge / 2);
									
									
		double[][] submatrix = new double[l�nge][l�nge]; 
															
		double summe;
		int index;

		for (int i = distanz; i < width - distanz; i++) {
			for (int j = distanz; j < height - distanz; j++) { // geht �ber alle
																// Pixel

				index = 0;

				for (int k = 0; k < l�nge; k++) {

					submatrix[index] = Arrays.copyOfRange(src[0][i - distanz
							+ k], j - distanz, j + distanz + 1); // f�llen der
																	// einzelnen
																	// Felder
					index++;
				}

				summe = 0;
				double summe2 = 0;
				for (int l = 0; l < l�nge; l++) {
					for (int m = 0; m < l�nge; m++) {

						summe = summe + (submatrix[m][l] * vertikal[l][m]);
						summe2 = summe2 + (submatrix[m][l] * horizontal[l][m]);
					}
					
					double gradient = Math.sqrt(summe*summe+summe2*summe2);
					
					if (gradient>max) max = gradient;
										
					dst[0][i][j] = gradient;
					dst[1][i][j] = gradient;
					dst[2][i][j] = gradient;
				}
			}
			
		}
		for (int n = 0; n < width; n++) {
			for (int o = 0; o < height; o++) {
				for (int k = 0; k < 3; k++) {
					dst[k][n][o] = (dst[k][n][o])*(255/(max));
				}
			}
		}
	}
}
