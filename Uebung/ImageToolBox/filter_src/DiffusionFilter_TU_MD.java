import sun.security.ssl.Debug;
import imageToolBox.AbstractFilter;

/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class DiffusionFilter_TU_MD extends AbstractFilter {

	public void filter(double[][][] src, double[][][] dst) {
		
		int iterations = (int)getDoubleProperty("Iterationen");
		double epsylon0 = getDoubleProperty("Epsylon");
		double lambda = getDoubleProperty("Lambda");
		
		double epsylon;
		
		double[] g = new double[2];
		double norm;
		double[][][][] j = new double[2][3][width][height];
		double[][][] div = new double[3][width][height];
		
		double[][] D = new double[2][2];
		
		for(int w=0; w<width; w++){
			for(int h=0; h<height; h++){
				for(int k = 0; k < 3; k++){
					dst[k][w][h] = src[k][w][h];
				}
			}
		}
		
		for(int it=0; it < iterations; it++){
			if(it == iterations-1){
				Debug.println("ok", "done");
			}
			for(int w=1; w<width-1; w++){
				for(int h=1; h<height-1; h++){
					for(int k = 0; k < 3; k++){
						//Gradient abschätzen durch Differenzen
						g[0] = dst[k][w+1][h]-dst[k][w-1][h];
						g[1] = dst[k][w][h+1]-dst[k][w][h-1];
						
						//Norm
						norm = Math.sqrt(Math.pow(g[0],2)+Math.pow(g[1],2));
						
						//Epsylon von Gradient bestimmen
						epsylon = epsylon0*Math.pow(lambda,2)/(Math.pow(norm,2)+Math.pow(lambda,2));
						
						//Diffusionstensor
						D[0][0] = epsylon;
						D[0][1] = 0;
						D[1][0] = 0;
						D[1][1] = epsylon;
						
						//Fluss
						j[0][k][w][h] = -D[0][0]*g[0];
						j[1][k][w][h] = -D[1][1]*g[1];
					}
				}
			}
			for(int w=1; w<width-1; w++){
				for(int h=1; h<height-1; h++){
					for(int k = 0; k < 3; k++){
						//Divergenz
						div[k][w][h] = j[0][k][w+1][h]-j[0][k][w-1][h]+j[1][k][w][h+1]-j[1][k][w][h-1];
						
						dst[k][w][h] = dst[k][w][h]-div[k][w][h];
					}
				}
			}
		}
	}
		
	public boolean hasProperties() {
		return true;
	}

	public void initGUI() {
		addDoubleProperty("Lambda", 1);
		addDoubleProperty("Epsylon", 1);
		addDoubleProperty("Iterationen", 500);
	}
	
}
