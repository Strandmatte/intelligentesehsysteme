import imageToolBox.AbstractFilter;

/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class Graufilter_TU_MD extends AbstractFilter {
	@Override
	public void filter(double[][][] src, double[][][] dst) {
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				for (int k = 0; k < 3; k++) {
					dst[k][i][j] = (src[0][i][j] + src[1][i][j] + src[2][i][j]) / 3;
				}
			}
		}
	}

}
