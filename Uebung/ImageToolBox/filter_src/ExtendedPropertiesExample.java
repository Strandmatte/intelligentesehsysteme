import imageToolBox.AbstractFilter;
import java.awt.*;

public class ExtendedPropertiesExample extends AbstractFilter
{
  TextField threshold;

  @Override
  public void filter(double[][][] src, double[][][] dest)
  {
    double thres = Double.parseDouble(threshold.getText());
    for (int i = 0; i < width; i++)
    {
      for (int j = 0; j < height; j++)
      {
        dest[0][i][j] = 0;
        dest[1][i][j] = 0;
        dest[2][i][j] = (src[0][i][j] > thres) ? src[0][i][j] : 0;
      }
    }
  }

  @Override
  public boolean hasProperties()
  {
    return true;
  }

  @Override
  public void initGUI()
  {
    Frame pf = getPropertiesFrame();
    pf.setSize(300, 125);
    pf.setLayout(null);
    
    threshold = new TextField();
    Label lblThreshold = new Label("blue threshold:");
    
    lblThreshold.setLocation(0, 0);
    lblThreshold.setSize(290, 25);
    threshold.setText("125.0");
    threshold.setLocation(5, 30);
    threshold.setSize(280, 25);
    
    pf.add(lblThreshold);
    pf.add(threshold);
  }
}
