import java.util.StringTokenizer;
import imageToolBox.AbstractFilter;

public class DataTransferExample2 extends AbstractFilter
{
  public void filter(double[][][] src, double[][][] dst)
  {
    String line;
    StringTokenizer tok = null;
    double randomNumber;
    
    for(int x = 0; x < width; x++)
    {
      line = readSrcDataLine();
      tok = new StringTokenizer(line);
      
      for(int y = 0; y < height; y++)
      {
        randomNumber = Double.parseDouble(tok.nextToken());
        for(int b = 0; b < 3; b++)
          dst[b][x][y] = src[b][x][y]/randomNumber;
      }
    }
  }
}
