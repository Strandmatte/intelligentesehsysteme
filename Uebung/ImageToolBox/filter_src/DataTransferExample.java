import imageToolBox.AbstractFilter;

public class DataTransferExample extends AbstractFilter
{
  public void filter(double[][][] src, double[][][] dst)
  {
    // Create a temporary file that is deleted 
	// after you terminate the ImageToolBox
    createDatafile();   
    // you can name your temporary files 
    // and make it permanently
    // createDatafile("testdata.txt", false)
    
    String line;
    double randomNumber;
    
    for(int x = 0; x < width; x++)
    {
      line = "";
      for(int y = 0; y < height; y++)
      {
        randomNumber = Math.max(Math.random(), 0.1);
        line += randomNumber + " ";
        for(int b = 0; b < 3; b++)
          dst[b][x][y] = randomNumber*src[b][x][y];
      }
      
      writeLine(line);
    }
  }
}
