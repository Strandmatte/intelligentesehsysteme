import imageToolBox.AbstractFilter;

/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 *
 */

public class Gammafilter_TU_MD extends AbstractFilter {
	@Override
	public void filter(double[][][] src, double[][][] dst) {

		double gamma = getDoubleProperty("gamma");
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				for (int k = 0; k < 3; k++) {
					/*I_min = 0 -> also Formel vereinfacht*/
					double basis = src[k][i][j]/255;
					dst[k][i][j] = 255*Math.pow(basis, gamma);
				}
			}
		}
	}

	public boolean hasProperties() {
		return true;
	}

	public void initGUI() {
		addDoubleProperty("gamma", 1);
	}
}
