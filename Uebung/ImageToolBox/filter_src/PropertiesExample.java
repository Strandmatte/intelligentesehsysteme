import imageToolBox.AbstractFilter;

public class PropertiesExample extends AbstractFilter
{
  @Override
  public void filter(double[][][] src, double[][][] dest)
  {
    double thres = getDoubleProperty("t");

    for (int i = 0; i < width; i++)
    {
      for (int j = 0; j < height; j++)
      {
        dest[0][i][j] = 0;
        dest[1][i][j] = 0;
        dest[2][i][j] = (src[0][i][j] > thres)?src[0][i][j]:0;
      }

    }
  }

  @Override
  public boolean hasProperties()
  {
    return true;
  }

  @Override
  public void initGUI()
  {
    addDoubleProperty("t", 30);
  }
}
