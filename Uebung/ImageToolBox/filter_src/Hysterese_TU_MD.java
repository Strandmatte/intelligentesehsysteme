import java.util.Arrays;

import imageToolBox.AbstractFilter;

/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class Hysterese_TU_MD extends AbstractFilter {
			int index = 0;
			double[][] submatrix = new double[3][3];
	@Override
	public void filter(double[][][] src, double[][][] dst) {

		//Schwellenwerte m�ssen aufsteigend eingegeben werden, da keine Sortierfunktion implementiert
		double tresh1 = getDoubleProperty("Schwellwert 1");
		double tresh2 = getDoubleProperty("Schwellwert 2");

		//Geht �ber alle Pixel und ordnet den einzelnen Regionen zu
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				for (int k = 0; k < 3; k++) {
					if (src[k][i][j] < tresh1) {
						dst[0][i][j] = 0;
						dst[1][i][j] = 0;
						dst[2][i][j] = 0;
					} else if (src[k][i][j] < tresh2) {
						dst[0][i][j] = 255;
						dst[1][i][j] = 0;
						dst[2][i][j] = 0;
					} else if (src[k][i][j] >= tresh2) {
						dst[0][i][j] = 255;
						dst[1][i][j] = 255;
						dst[2][i][j] = 255;
					}
				}
			}
		}
		

		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				if(dst[0][i][j] == 255){
					if(dst[1][i][j] == 255){
					}
					else
						for (int k = 0; k < 3; k++) {
							submatrix[index] = Arrays.copyOfRange(dst[2][i - 1
									+ k], j - 1, j + 1 + 1);
							index++;
						}
					index = 0;
					loop:
					for (int l = 0; l < 3; l++) {
						for (int m = 0; m < 3; m++) {
							if(submatrix[l][m]==255){
								dst[0][i][j] = 255;
								dst[1][i][j] = 255;
								dst[2][i][j] = 255;
								break loop;
							}
						}
						dst[0][i][j] = 0;
						dst[1][i][j] = 0;
						dst[2][i][j] = 0;
					}
				}
			}
		}
	}

	public boolean hasProperties() {
		return true;
	}

	public void initGUI() {
		addDoubleProperty("Schwellwert 1", 0);
		addDoubleProperty("Schwellwert 2", 0);
	}
}