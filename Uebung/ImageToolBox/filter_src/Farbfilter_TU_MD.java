import imageToolBox.AbstractFilter;

/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class Farbfilter_TU_MD extends AbstractFilter {
	@Override
	public void filter(double[][][] src, double[][][] dst) {

		boolean rot = getBooleanProperty("rot");
		boolean gr�n = getBooleanProperty("gr�n");
		boolean blau = getBooleanProperty("blau");

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {

				if (rot == false) {
					dst[0][i][j] = 0;
				} else {
					dst[0][i][j] = src[0][i][j];
				}
				if (gr�n == false) {
					dst[1][i][j] = 0;
				} else {
					dst[1][i][j] = src[1][i][j];
				}
				if (blau == false) {
					dst[2][i][j] = 0;
				} else {
					dst[2][i][j] = src[2][i][j];
				}
			}
		}
	}

	public boolean hasProperties() {
		return true;
	}

	public void initGUI() {
		addBooleanProperty("rot", false);
		addBooleanProperty("gr�n", true);
		addBooleanProperty("blau", true);
	}
}
