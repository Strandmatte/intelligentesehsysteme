import imageToolBox.AbstractFilter;

import java.util.Arrays;

/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class Sobelfilter2_TU_MD extends AbstractFilter {

	@Override
	public void filter(double[][][] src, double[][][] dst) {
		// Sobel Masken
		double[][] horizontal = { { -1.0, 0.0, 1.0 }, { -2.0, 0.0, 2.0 },
				{ -1.0, 0.0, 1.0 } };
		double[][] vertikal = { { 1.0, 2.0, 1.0 }, { 0.0, 0.0, 0.0 },
				{ -1.0, -2.0, -1.0 } };
		// Array zum Mitf�hren der Gradientenbetr�ge & -richtungen
		double[][][] temp = new double[2][width][height];
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				for (int k = 0; k < 2; k++) {
					temp[k][i][j] = src[k][i][j];
				}
			}
		}

		// f�r die sp�tere Grauwertspreizung
		double max = 255;
		double min = 255;

		int l�nge = 3;
		int distanz = (l�nge / 2);

		// genau wie beim Convolutionfilter
		double[][] submatrix = new double[l�nge][l�nge];

		int index;

		for (int i = distanz; i < width - distanz; i++) {
			for (int j = distanz; j < height - distanz; j++) {
				index = 0;
				for (int k = 0; k < l�nge; k++) {
					submatrix[index] = Arrays.copyOfRange(src[0][i - distanz
							+ k], j - distanz, j + distanz + 1);
					index++;
				}

				double summeVertikal = 0;
				double summeHorizontal = 0;
				for (int l = 0; l < l�nge; l++) {
					for (int m = 0; m < l�nge; m++) {
						// berechnet die Werte f�r einen Pixel je Maske
						summeVertikal = summeVertikal
								+ (submatrix[m][l] * vertikal[l][m]);
						summeHorizontal = summeHorizontal
								+ (submatrix[m][l] * horizontal[l][m]);
					}
					// Gradientenberechnung
					double gradient = Math.sqrt(summeVertikal * summeVertikal
							+ summeHorizontal * summeHorizontal);
					// Winkelbestimmung
					double winkel = Math.atan2(Math.abs(summeVertikal),
							Math.abs(summeHorizontal));
					winkel = winkel * 360 / Math.PI;
					// falls negativer winkel, umrechnung auf positiv
					if (winkel < 0) {
						winkel = 360 + winkel;
					}

					double ratio = winkel / 45.0;
					String s = String.valueOf(ratio);
					double nachkomma = Double.parseDouble(s.substring(2, 3));

					if (nachkomma >= 5) {
						ratio = (int) ratio;
						ratio = ratio + 1;
					} else
						ratio = (int) ratio;
					// Anpassen der Winkel
					winkel = ratio * 45;
					if (winkel == 180)
						winkel = 0;
					if (winkel == 225)
						winkel = 45;
					if (winkel == 270)
						winkel = 90;
					if (winkel == 315)
						winkel = 135;

					// Aktualisierung MaxGiven
					if (gradient > max)
						max = gradient;
					if (gradient < min)
						min = gradient;
					// Ausgabe aktualisieren
					dst[0][i][j] = gradient;
					dst[1][i][j] = gradient;
					dst[2][i][j] = gradient;
					// Mitf�hren St�rke & Richtung
					temp[0][i][j] = gradient;
					temp[1][i][j] = winkel;
				}
			}

		}
		// Grauwertspreizung
		for (int n = 0; n < width; n++) {
			for (int o = 0; o < height; o++) {
				for (int k = 0; k < 3; k++) {
					dst[k][n][o] = (dst[k][n][o] - min) * (255 / (max - min));
				}
			}
		}
		// Ausd�nnen der Kanten
		for (int i = distanz; i < width - distanz; i++) {
			for (int j = distanz; j <  height - distanz; j++) {

				if (temp[1][i][j] == 0) {
					if ((temp[0][i - 1][j] > temp[0][i][j] && temp[1][i - 1][j] == 0)
							|| (temp[0][i + 1][j] > temp[0][i][j] && temp[1][i + 1][j] == 0)){
						dst[0][i][j] = 0;
						dst[1][i][j] = 0;
						dst[2][i][j] = 0;
					}
				} else if (temp[1][i][j] == 135) {
					if ((temp[0][i - 1][j - 1] > temp[0][i][j] && temp[1][i-1][j-1] == 135)
							|| (temp[0][i + 1][j + 1] > temp[0][i][j] && temp[1][i+1][j+1] == 135)) {
						dst[0][i][j] = 0;
						dst[1][i][j] = 0;
						dst[2][i][j] = 0;
					}
				} else if (temp[1][i][j] == 90) {
					if ((temp[0][i][j - 1] > temp[0][i][j] && temp[1][i][j-1] == 90)
							|| (temp[0][i][j + 1] > temp[0][i][j] && temp[1][i][j+1] == 90)) {
						dst[0][i][j] = 0;
						dst[1][i][j] = 0;
						dst[2][i][j] = 0;
					}
				} else if (temp[1][i][j] == 45) {
					if ((temp[0][i + 1][j - 1] > temp[0][i][j] && temp[1][i+1][j-1] == 45)
							|| (temp[0][i - 1][j + 1] > temp[0][i][j] && temp[1][i-1][j+1] == 45)) {
						dst[0][i][j] = 0;
						dst[1][i][j] = 0;
						dst[2][i][j] = 0;
					}
				}
			}
		}
	}
}