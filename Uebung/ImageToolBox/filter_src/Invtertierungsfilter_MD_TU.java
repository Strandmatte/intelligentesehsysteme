import imageToolBox.AbstractFilter;

/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class Invtertierungsfilter_MD_TU extends AbstractFilter {
	@Override
	public void filter(double[][][] src, double[][][] dst) {

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				dst[0][i][j] = 255-src[0][i][j];
				dst[1][i][j] = 255-src[1][i][j];
				dst[2][i][j] = 255-src[2][i][j];
			}
		}
	}
}
