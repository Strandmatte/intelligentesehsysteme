import java.awt.Point;
import java.util.LinkedList;
import imageToolBox.AbstractFilter;

/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class InteractiveRegionGrowing_TU_MD extends AbstractFilter {

	private Point p;
	private double min = 0;
	private double max = 0;
	private double[] color = new double[3];

	@Override
	public void handleMouseClick(Point p) {
		System.out.println(p);
		this.p = p;
	}

	@Override
	public void filter(double[][][] src, double[][][] dst) {
		double variance = getDoubleProperty("variance");
		//umfärben alter eingefärbter Bereiche für Erkennung
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				dst[0][i][j] = src[0][i][j];
				dst[1][i][j] = src[1][i][j];
				dst[2][i][j] = src[0][i][j];
			}
		}
		//erzeugung von Zufallsfarben 
		getColor();
		//suche nach Point p & floodFill anwenden
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (Math.abs(x - p.x) == 0 && Math.abs(y - p.y) == 0) {
					double targetColor = src[0][x][y];
					min = targetColor - variance;
					max = targetColor + variance;
					floodFill(dst, p.x, p.y, targetColor);
				}
			}
		}
	}
	
	//gibt Rnd Farben 1, 2, 3 zurück
	public void getColor() {
		int color1 = (int) (Math.random() * (255 + 1));
		int color2 = (int) (Math.random() * (255 + 1));
		if (color2 == color1)
			color2 = color1 + 2;
		int color3 = (int) (Math.random() * (255 + 1));
		if (color3 == color1)
			color3 = color1 - 2;
		color[0] = color1;
		color[1] = color2;
		color[2] = color3;
	}

	//entspricht Pseudocode der Aufgabe
	public void floodFill(double[][][] dst, int i, int j,
			double targetColor) {
		LinkedList<int[]> queue = new LinkedList<int[]>();
		int[] node = { i, j };
		queue.add(node);
		while (queue.size() > 0) {
			int[] currentNode = queue.remove();
			int x = currentNode[0];
			int y = currentNode[1];
			if (hasSimiliarColor(dst, x, y) == true) {
				dst[0][x][y] = color[0];
				dst[1][x][y] = color[1];
				dst[2][x][y] = color[2];
				if (y > 0) {
					if (hasSimiliarColor(dst, x, y - 1) == true) {
						int[] nodeToAdd = { x, y - 1 };
						queue.add(nodeToAdd);
					}
				}
				if (x < width - 1 && y > 0) {
					if (hasSimiliarColor(dst, x + 1, y - 1) == true) {
						int[] nodeToAdd = { x + 1, y - 1 };
						queue.add(nodeToAdd);
					}
				}
				if (x < width - 1) {
					if (hasSimiliarColor(dst, x + 1, y) == true) {
						int[] nodeToAdd = { x + 1, y };
						queue.add(nodeToAdd);
					}
				}
				if (x < width - 1 && y < height - 1) {
					if (hasSimiliarColor(dst, x + 1, y + 1) == true) {
						int[] nodeToAdd = { x + 1, y + 1 };
						queue.add(nodeToAdd);
					}
				}
				if (y < height - 1) {
					if (hasSimiliarColor(dst, x, y + 1) == true) {
						int[] nodeToAdd = { x, y + 1 };
						queue.add(nodeToAdd);
					}
				}
				if (x > 0 && y < height - 1) {
					if (hasSimiliarColor(dst, x - 1, y + 1) == true) {
						int[] nodeToAdd = { x - 1, y + 1 };
						queue.add(nodeToAdd);
					}
				}
				if (x > 0) {
					if (hasSimiliarColor(dst, x - 1, y) == true) {
						int[] nodeToAdd = { x - 1, y };
						queue.add(nodeToAdd);
					}
				}
				if (x > 0 && y > 0) {
					if (hasSimiliarColor(dst, x - 1, y - 1) == true) {
						int[] nodeToAdd = { x - 1, y - 1 };
						queue.add(nodeToAdd);
					}
				}
			}
		}
		return;
	}
	//Test auf ähnliche Farbe
	private boolean hasSimiliarColor(double[][][] dst, int i, int j) {
		//Homogenitätskriterium
		if ((dst[0][i][j] > min && dst[0][i][j] < max)
				&& (dst[1][i][j] > min && dst[1][i][j] < max)
				&& (dst[2][i][j] > min && dst[2][i][j] < max))
		{
			return true;
		}
		//Erkennung alter Einfärbungen
		if ((dst[0][i][j] != dst[1][i][j]) && dst[0][i][j] == dst[2][i][j])
			return true;
		return false;
	}

	@Override
	public boolean hasProperties() {
		return true;
	}

	@Override
	public void initGUI() {
		addDoubleProperty("variance", 25);
	}
}
