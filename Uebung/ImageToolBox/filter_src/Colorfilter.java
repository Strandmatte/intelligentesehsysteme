import imageToolBox.AbstractFilter;

public class Colorfilter extends AbstractFilter
{
  @Override
  public void filter(double[][][] src, double[][][] dest)
  {
    for(int i = 0; i < width; i++)
    {
      for(int j = 0; j < height; j++)
      {
        dest[0][i][j] = 0;
        dest[1][i][j] = 0;
        dest[2][i][j] = src[2][i][j];
      }
    }
  }
}
