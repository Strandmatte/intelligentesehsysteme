import java.awt.Point;
import java.util.Arrays;

import imageToolBox.AbstractFilter;


public class InteraktiveKantenverfolgung_TU_MD extends AbstractFilter {
	
	private boolean[][] found;
	
	// Array zum Mitf�hren der Gradientenbetr�ge & -richtungen
	double[][][] gradients;
	
	private Point p;
	
	boolean goOn;
	boolean[][] processed;
	
	private double sensitivity;
	
	@Override
	public void handleMouseClick(Point p) {
		System.out.println(p);
		this.p = p;
	}
	
	@Override
	public void filter(double[][][] src, double[][][] dst) {
		
		sensitivity = getDoubleProperty("sensitivity");
		
		calcGradients(src);
		
		for (int x=0; x<width; x++) {
			for (int y=0; y<height; y++) {
				for (int k=0; k<3; k++) {
					dst[k][x][y] = src[k][x][y];
				}
			}
		}
		
		int x = p.x-2;
		int y = p.y-2;
		
		while(x < 0) x++;
		while(y < 0) y++;
		int tmpY = y;
		
		Point maxG = p;
		
		while(x <= p.x+2 && x <= width){
			while(y <= p.y+2 && x <= height){
				if(gradients[0][x][y] > gradients[0][p.x][p.y]){
					maxG.x = x;
					maxG.y = y;
				}
				y++;
			}
			y = tmpY;
			x++;
		}
		
		p.x = maxG.x;
		p.y = maxG.y;
		
		System.out.println(p);
		
		x = p.x-3;
		y = p.y-3;
		
		while(x < 0) x++;
		while(y < 0) y++;
		while(x <= p.x+3 && x <= width){
			dst[0][x][p.y] = 255;	
			dst[1][x][p.y] = 0;			
			dst[2][x][p.y] = 0;
			x++;
		}
		while(y <= p.y+3 && y <= height){
			dst[0][p.x][y] = 255;	
			dst[1][p.x][y] = 0;			
			dst[2][p.x][y] = 0;				
			y++;
		}
		
		found = new boolean[width][height];
		processed = new boolean[width][height];
		for(int i = 0; i < width; i++){
			for(int j = 0; j < height; j++){
				found[i][j] = false;
				processed[i][j] = true;
			}
		}
		goOn = true;
		search(dst,p);
	}
	
	void calcGradients(double[][][] src){
		gradients = new double[2][width][height];
		// Sobel Masken
		double[][] horizontal = { { -1.0, 0.0, 1.0 }, { -2.0, 0.0, 2.0 },
				{ -1.0, 0.0, 1.0 } };
		double[][] vertikal = { { 1.0, 2.0, 1.0 }, { 0.0, 0.0, 0.0 },
				{ -1.0, -2.0, -1.0 } };
		// f�r die sp�tere Grauwertspreizung
		double max = 255;
		double min = 255;
		int l�nge = 3;
		int distanz = (l�nge / 2);
		// f�r Gradient & Richtung
		double[][] submatrix = new double[l�nge][l�nge];
		int index;

		for (int i = distanz; i < width - distanz; i++) {
			for (int j = distanz; j < height - distanz; j++) {
				index = 0;
				for (int k = 0; k < l�nge; k++) {
					submatrix[index] = Arrays.copyOfRange(src[0][i - distanz
							+ k], j - distanz, j + distanz + 1);
					index++;
				}

				double summeVertikal = 0;
				double summeHorizontal = 0;
				for (int l = 0; l < l�nge; l++) {
					for (int m = 0; m < l�nge; m++) {
						// berechnet die Werte f�r einen Pixel je Maske
						summeVertikal = summeVertikal
								+ (submatrix[m][l] * vertikal[l][m]);
						summeHorizontal = summeHorizontal
								+ (submatrix[m][l] * horizontal[l][m]);
					}
					// Gradientenberechnung
					double gradient = (int) Math
							.sqrt(summeVertikal * summeVertikal
									+ summeHorizontal * summeHorizontal);
					// Winkelbestimmung
					int winkel = (int) Math.atan(summeVertikal
							/ summeHorizontal);
					winkel = (int) (winkel * 360 / Math.PI);

					if (winkel < 22.5 || (winkel >= 157.5 && winkel < 202.5)
							|| winkel >= 337.5) {
						winkel = 0;
					} else if ((winkel >= 22.5 && winkel < 67.5)
							|| (winkel >= 202.5 && winkel < 247.5)) {
						winkel = 45;
					} else if ((winkel >= 67.5 && winkel < 112.5)
							|| (winkel >= 247.5 && winkel < 292.5)) {
						winkel = 90;
					} else if ((winkel >= 112.5 && winkel < 157.5)
							|| (winkel >= 292.5 && winkel < 337.5)) {
						winkel = 135;
					}
					// Aktualisierung MaxGiven
					if (gradient > max)
						max = gradient;
					if (gradient < min)
						min = gradient;
					// Ausgabe aktualisieren
					// Mitf�hren St�rke & Richtung
					gradients[0][i][j] = gradient;
					gradients[1][i][j] = winkel;
				}
			}

		}
		// Grauwertspreizung
		for (int n = 0; n < width; n++) {
			for (int o = 0; o < height; o++) {
				gradients[0][n][o] = (int) ((gradients[0][n][o] - min) * (255 / (max - min)));
			}
		}
	}
	
	void search(double[][][] dst, Point s){
		while(goOn){
			goOn = false;
			if(gradients[1][s.x][s.y] == 270) searchNorth(dst,s);
			if(gradients[1][s.x][s.y] == 315) searchNorthEast(dst,s);
			if(gradients[1][s.x][s.y] == 0) searchEast(dst,s);
			if(gradients[1][s.x][s.y] == 45) searchSouthEast(dst,s);
			if(gradients[1][s.x][s.y] == 90) searchSouth(dst,s);
			if(gradients[1][s.x][s.y] == 135) searchSouthWest(dst,s);
			if(gradients[1][s.x][s.y] == 180) searchWest(dst,s);
			if(gradients[1][s.x][s.y] == 225) searchNorthWest(dst,s);
			
			outerLoop: for(int i = 0; i < width; i++){
				for(int j = 0; j < height; j++){
					if(processed[i][j] == false){
						s.x = i;
						s.y = j;
						goOn = true;
						break outerLoop;
					}
				}
			}
		}
	}
	
	void searchNorth(double[][][] dst, Point q){
		
		Point pR = new Point();
		pR.x = q.x;
		pR.y = q.y;
		
		processed[q.x][q.y] = true;
		
		int x;
		int y;
		for(y = pR.y-2; y < pR.y; y++){
			if(y < 0){
				y = 0;
			}
			if(y == pR.y-2){
				for(x = pR.x-2; x < pR.x+3 && x < width; x++){
					if(x < 0) x++;
					else if(gradients[1][x][y] == gradients[1][pR.x][pR.y] && gradients[0][x][y] > gradients[0][pR.x][pR.y] - sensitivity && gradients[0][x][y] < gradients[0][pR.x][pR.y] + sensitivity && found[x][y] == false){
						colorMeRed(x,y,dst);
						found[x][y] = true;
						processed[x][y] = false;
					}
				}
			}
			if(y == pR.y-1){
				for(x = pR.x-1; x < pR.x+2 && x < width; x++){
					if(x < 0) x++;
					else if(gradients[1][x][y] == gradients[1][pR.x][pR.y] && gradients[0][x][y] > gradients[0][pR.x][pR.y] - sensitivity && gradients[0][x][y] < gradients[0][pR.x][pR.y] + sensitivity && found[x][y] == false){
						colorMeRed(x,y,dst);
						found[x][y] = true;
						processed[x][y] = false;
					}
				}
			}
		}
	}
	
	void searchEast(double[][][] dst, Point q){
		
		Point pR = new Point();
		pR.x = q.x;
		pR.y = q.y;
		
		processed[q.x][q.y] = true;
		
		int x;
		int y;
		for(x = pR.x+2; x > pR.x; x--){
			if(x >= width){
				x = width-1;
			}
			if(x == pR.x+2){
				for(y = pR.y-2; y < pR.y+3 && y < height; y++){
					if(y < 0) y++;
					else if(gradients[1][x][y] == gradients[1][pR.x][pR.y] && gradients[0][x][y] > gradients[0][pR.x][pR.y] - sensitivity && gradients[0][x][y] < gradients[0][pR.x][pR.y] + sensitivity && found[x][y] == false){
						colorMeRed(x,y,dst);
						found[x][y] = true;
						processed[x][y] = false;
					}
				}
			}
			if(x == pR.x+1){
				for(y = pR.y-1; y < pR.y+2 && y < height; y++){
					if(y < 0) y++;
					else if(gradients[1][x][y] == gradients[1][pR.x][pR.y] && gradients[0][x][y] > gradients[0][pR.x][pR.y] - sensitivity && gradients[0][x][y] < gradients[0][pR.x][pR.y] + sensitivity && found[x][y] == false){
						colorMeRed(x,y,dst);
						found[x][y] = true;
						processed[x][y] = false;
					}
				}
			}
		}
	}

	void searchSouth(double[][][] dst, Point q){
		
		Point pR = new Point();
		pR.x = q.x;
		pR.y = q.y;
		
		processed[q.x][q.y] = true;
		
		int x;
		int y;
		for(y = pR.y+2; y > pR.y; y--){
			if(y >= height){
				y = height-1;
			}
			if(y == pR.y+2){
				for(x = pR.x-2; x < pR.x+3 && x < width; x++){
					if(x < 0) x++;
					else if(gradients[1][x][y] == gradients[1][pR.x][pR.y] && gradients[0][x][y] > gradients[0][pR.x][pR.y] - sensitivity && gradients[0][x][y] < gradients[0][pR.x][pR.y] + sensitivity && found[x][y] == false){
						colorMeRed(x,y,dst);
						found[x][y] = true;
						processed[x][y] = false;
					}
				}
			}
			if(y == pR.y+1){
				for(x = pR.x-1; x < pR.x+2 && x < width; x++){
					if(x < 0) x++;
					else if(gradients[1][x][y] == gradients[1][pR.x][pR.y] && gradients[0][x][y] > gradients[0][pR.x][pR.y] - sensitivity && gradients[0][x][y] < gradients[0][pR.x][pR.y] + sensitivity && found[x][y] == false){
						colorMeRed(x,y,dst);
						found[x][y] = true;
						processed[x][y] = false;
					}
				}
			}
		}
	}

	void searchWest(double[][][] dst, Point q){
		
		Point pR = new Point();
		pR.x = q.x;
		pR.y = q.y;
		
		processed[q.x][q.y] = true;
		
		int x;
		int y;
		for(x = pR.x-2; x < pR.x; x++){

			if(x < 0){
				x = 0;
			}
			if(x == pR.x-2){
				for(y = pR.y-2; y < pR.y+3 && y < height; y++){
					if(y < 0) y++;
					else if(gradients[1][x][y] == gradients[1][pR.x][pR.y] && gradients[0][x][y] > gradients[0][pR.x][pR.y] - sensitivity && gradients[0][x][y] < gradients[0][pR.x][pR.y] + sensitivity && found[x][y] == false){
						colorMeRed(x,y,dst);
						found[x][y] = true;
						processed[x][y] = false;
					}
				}
			}
			if(x == pR.x-1){
				for(y = pR.y-1; y < pR.y+2 && y < height; y++){
					if(y < 0) y++;
					else if(gradients[1][x][y] == gradients[1][pR.x][pR.y] && gradients[0][x][y] > gradients[0][pR.x][pR.y] - sensitivity && gradients[0][x][y] < gradients[0][pR.x][pR.y] + sensitivity && found[x][y] == false){
						colorMeRed(x,y,dst);
						found[x][y] = true;
						processed[x][y] = false;
					}
				}
			}
		}
	}

	void searchNorthEast(double[][][] dst, Point q){
		
		Point pR = new Point();
		pR.x = q.x;
		pR.y = q.y;
		
		processed[q.x][q.y] = true;
		
		for(int x = pR.x+1; x < pR.x+3 && x < width; x++){
			for(int y = pR.y-1; y > pR.y-3 && y >= 0; y--){
				if(gradients[1][x][y] == gradients[1][pR.x][pR.y] && gradients[0][x][y] > gradients[0][pR.x][pR.y] - sensitivity && gradients[0][x][y] < gradients[0][pR.x][pR.y] + sensitivity && found[x][y] == false){
					if(found[x][y] == false){
						colorMeRed(x,y,dst);
						found[x][y] = true;
						processed[x][y] = false;
					}
				}
			}
		}	
	}

	void searchSouthEast(double[][][] dst, Point q){
		
		Point pR = new Point();
		pR.x = q.x;
		pR.y = q.y;
		
		processed[q.x][q.y] = true;
		
		for(int x = pR.x+1; x < pR.x+3 && x < width; x++){
			for(int y = pR.y+1; y < pR.y+3 && y < height; y++){
				if(gradients[1][x][y] == gradients[1][pR.x][pR.y] && gradients[0][x][y] > gradients[0][pR.x][pR.y] - sensitivity && gradients[0][x][y] < gradients[0][pR.x][pR.y] + sensitivity && found[x][y] == false){
					if(found[x][y] == false){
						colorMeRed(x,y,dst);
						found[x][y] = true;
						processed[x][y] = false;
					}
				}
			}
		}	
	}

	void searchSouthWest(double[][][] dst, Point q){
		
		Point pR = new Point();
		pR.x = q.x;
		pR.y = q.y;
		
		processed[q.x][q.y] = true;
		
		for(int x = pR.x-1; x > pR.x-3 && x >= 0; x--){
			for(int y = pR.y+1; y < pR.y+3 && y < height; y++){
				if(gradients[1][x][y] == gradients[1][pR.x][pR.y] && gradients[0][x][y] > gradients[0][pR.x][pR.y] - sensitivity && gradients[0][x][y] < gradients[0][pR.x][pR.y] + sensitivity && found[x][y] == false){
					if(found[x][y] == false){
						colorMeRed(x,y,dst);
						found[x][y] = true;
						processed[x][y] = false;
					}
				}
			}
		}	
	}

	void searchNorthWest(double[][][] dst, Point q){
		
		Point pR = new Point();
		pR.x = q.x;
		pR.y = q.y;
		
		processed[q.x][q.y] = true;
		
		for(int x = pR.x-1; x > pR.x-3 && x >= 0; x--){
			for(int y = pR.y-1; y > pR.y-3 && y >= 0; y--){
				if(gradients[1][x][y] == gradients[1][pR.x][pR.y] && gradients[0][x][y] > gradients[0][pR.x][pR.y] - sensitivity && gradients[0][x][y] < gradients[0][pR.x][pR.y] + sensitivity && found[x][y] == false){
					if(found[x][y] == false){
						colorMeRed(x,y,dst);
						found[x][y] = true;
						processed[x][y] = false;
					}
				}
			}
		}	
	}
	
	void colorMeRed(int x, int y, double[][][] dst){
		dst[0][x][y]= 255;
		dst[1][x][y]= 0;
		dst[2][x][y]= 0; 
	}

	public boolean hasProperties() {
		return true;
	}

	public void initGUI() {
		addDoubleProperty("sensitivity", 15.0);
	}
}
