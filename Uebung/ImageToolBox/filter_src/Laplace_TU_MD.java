import imageToolBox.AbstractFilter;


public class Laplace_TU_MD extends AbstractFilter {
	
	@Override
	public void filter(double[][][] src, double[][][] dst) {
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				for (int k = 0; k < 3; k++) {
					src[k][i][j] = (src[0][i][j] + src[1][i][j] + src[2][i][j]) / 3;
				}
			}
		}
		
		boolean L4 = getBooleanProperty("L4-Variante");
		
		double[][][] ImgLeft = new double[3][width][height];
		double[][][] ImgDown = new double[3][width][height];
		double[][][] ImgTopR = new double[3][width][height];
		double[][][] ImgDownR = new double[3][width][height];
		
		for(int w = 0; w < width-1; w++){
			for(int h = 0; h < height; h++){
				
				ImgLeft[0][w][h] = src[0][w+1][h];
				ImgLeft[1][w][h] = src[1][w+1][h];
				ImgLeft[2][w][h] = src[2][w+1][h];
				
			}
		}
		
		for(int w = 0; w < width; w++){
			for(int h = height-1; h > 0; h--){
				
				ImgDown[0][w][h] = src[0][w][h-1];
				ImgDown[1][w][h] = src[1][w][h-1];
				ImgDown[2][w][h] = src[2][w][h-1];
				
			}
		}
		
		if(L4){
			double[][]Laplace = {
				{0.0, 1.0, 0.0},
				{1.0, -4.0, 1.0},
				{0.0, 1.0, 0.0}
			};
			
			applyLaplace(Laplace,ImgLeft,ImgDown,src,dst);
		}
		else{
			double[][]Laplace = {
				{1.0, 1.0, 1.0},
				{1.0, -8.0, 1.0},
				{1.0, 1.0, 1.0}
			};
			
			for(int w = width-1; w >0; w--){
				for(int h = 0; h < height; h++){
					
					ImgTopR[0][w][h] = src[0][w-1][h];
					ImgTopR[1][w][h] = src[1][w-1][h];
					ImgTopR[2][w][h] = src[2][w-1][h];
					
				}
			}
			
			for(int w = 0; w < width; w++){
				for(int h = 0; h < height-1; h++){
					
					ImgTopR[0][w][h] = ImgTopR[0][w][h+1];
					ImgTopR[1][w][h] = ImgTopR[1][w][h+1];
					ImgTopR[2][w][h] = ImgTopR[2][w][h+1];
					
				}
			}
			
			for(int w = width-1; w >0; w--){
				for(int h = 0; h < height; h++){
					
					ImgDownR[0][w][h] = src[0][w-1][h];
					ImgDownR[1][w][h] = src[1][w-1][h];
					ImgDownR[2][w][h] = src[2][w-1][h];
					
				}
			}
			
			for(int w = 0; w < width; w++){
				for(int h = height-1; h > 0; h--){
					
					ImgDownR[0][w][h] = ImgDownR[0][w][h-1];
					ImgDownR[1][w][h] = ImgDownR[1][w][h-1];
					ImgDownR[2][w][h] = ImgDownR[2][w][h-1];
					
				}
			}

			applyLaplace(Laplace,ImgLeft,ImgDown,ImgTopR,ImgDownR,src,dst);
		}
		
	}
	
	public void applyLaplace(double[][]Laplace,double[][][] ImgLeft,double[][][] ImgDown,double[][][] src, double[][][] dst){
		
		double s1;
		double s2;
		double s3;
		
		for(int w = 1; w < width-1; w++){
			for(int h = 1; h < height-1; h++){
				for(int k = 0; k < 3; k++){
					s1 = Laplace[0][0]*src[k][w+1][h-1]+Laplace[1][0]*src[k][w+1][h]+Laplace[2][0]*src[k][w+1][h+1]
							+Laplace[0][1]*src[k][w][h-1]+Laplace[1][1]*src[k][w][h]+Laplace[2][1]*src[k][w][h+1]
							+Laplace[0][2]*src[k][w-1][h-1]+Laplace[1][2]*src[k][w-1][h]+Laplace[2][2]*src[k][w-1][h+1];
					
					s2 = Laplace[0][0]*ImgLeft[k][w+1][h-1]+Laplace[1][0]*ImgLeft[k][w+1][h]+Laplace[2][0]*ImgLeft[k][w+1][h+1]
							+Laplace[0][1]*ImgLeft[k][w][h-1]+Laplace[1][1]*ImgLeft[k][w][h]+Laplace[2][1]*ImgLeft[k][w][h+1]
							+Laplace[0][2]*ImgLeft[k][w-1][h-1]+Laplace[1][2]*ImgLeft[k][w-1][h]+Laplace[2][2]*ImgLeft[k][w-1][h+1];
					
					s3 = Laplace[0][0]*ImgDown[k][w+1][h-1]+Laplace[1][0]*ImgDown[k][w+1][h]+Laplace[2][0]*ImgDown[k][w+1][h+1]
							+Laplace[0][1]*ImgDown[k][w][h-1]+Laplace[1][1]*ImgDown[k][w][h]+Laplace[2][1]*ImgDown[k][w][h+1]
							+Laplace[0][2]*ImgDown[k][w-1][h-1]+Laplace[1][2]*ImgDown[k][w-1][h]+Laplace[2][2]*ImgDown[k][w-1][h+1];
					
					if((s1 > 0 && (s2 < 0 || s3 < 0)) || (s1 < 0 && (s2 > 0 || s3 > 0))){
						dst[k][w][h] = s1;
					}
					else{
						dst[k][w][h] = 0;
					}
				}
			}
		}
		
	}
	
	public void applyLaplace(double[][]Laplace,double[][][] ImgLeft,double[][][] ImgDown,double[][][] ImgTopR,double[][][] ImgDownR,double[][][] src, double[][][] dst){
		double s1;
		double s2;
		double s3;
		double s4;
		double s5;
		
		for(int w = 1; w < width-1; w++){
			for(int h = 1; h < height-1; h++){
				for(int k = 0; k < 3; k++){
					s1 = Laplace[0][0]*src[k][w+1][h-1]+Laplace[1][0]*src[k][w+1][h]+Laplace[2][0]*src[k][w+1][h+1]
							+Laplace[0][1]*src[k][w][h-1]+Laplace[1][1]*src[k][w][h]+Laplace[2][1]*src[k][w][h+1]
							+Laplace[0][2]*src[k][w-1][h-1]+Laplace[1][2]*src[k][w-1][h]+Laplace[2][2]*src[k][w-1][h+1];
					
					s2 = Laplace[0][0]*ImgLeft[k][w+1][h-1]+Laplace[1][0]*ImgLeft[k][w+1][h]+Laplace[2][0]*ImgLeft[k][w+1][h+1]
							+Laplace[0][1]*ImgLeft[k][w][h-1]+Laplace[1][1]*ImgLeft[k][w][h]+Laplace[2][1]*ImgLeft[k][w][h+1]
							+Laplace[0][2]*ImgLeft[k][w-1][h-1]+Laplace[1][2]*ImgLeft[k][w-1][h]+Laplace[2][2]*ImgLeft[k][w-1][h+1];
					
					s3 = Laplace[0][0]*ImgDown[k][w+1][h-1]+Laplace[1][0]*ImgDown[k][w+1][h]+Laplace[2][0]*ImgDown[k][w+1][h+1]
							+Laplace[0][1]*ImgDown[k][w][h-1]+Laplace[1][1]*ImgDown[k][w][h]+Laplace[2][1]*ImgDown[k][w][h+1]
							+Laplace[0][2]*ImgDown[k][w-1][h-1]+Laplace[1][2]*ImgDown[k][w-1][h]+Laplace[2][2]*ImgDown[k][w-1][h+1];
					
					s4 = Laplace[0][0]*ImgTopR[k][w+1][h-1]+Laplace[1][0]*ImgTopR[k][w+1][h]+Laplace[2][0]*ImgTopR[k][w+1][h+1]
							+Laplace[0][1]*ImgTopR[k][w][h-1]+Laplace[1][1]*ImgTopR[k][w][h]+Laplace[2][1]*ImgTopR[k][w][h+1]
							+Laplace[0][2]*ImgTopR[k][w-1][h-1]+Laplace[1][2]*ImgTopR[k][w-1][h]+Laplace[2][2]*ImgTopR[k][w-1][h+1];
					
					s5 = Laplace[0][0]*ImgDownR[k][w+1][h-1]+Laplace[1][0]*ImgDownR[k][w+1][h]+Laplace[2][0]*ImgDownR[k][w+1][h+1]
							+Laplace[0][1]*ImgDownR[k][w][h-1]+Laplace[1][1]*ImgDownR[k][w][h]+Laplace[2][1]*ImgDownR[k][w][h+1]
							+Laplace[0][2]*ImgDownR[k][w-1][h-1]+Laplace[1][2]*ImgDownR[k][w-1][h]+Laplace[2][2]*ImgDownR[k][w-1][h+1];
					
					if((s1 > 0 && (s2 < 0 || s3 < 0 || s4 < 0 || s5 < 0)) || (s1 < 0 && (s2 > 0 || s3 > 0 || s4 > 0 || s5 > 0))){
						dst[k][w][h] = s1;
					}
					else{
						dst[k][w][h] = 0;
					}
				}
			}
		}
	}
	
	public boolean hasProperties() {
		return true;
	}

	public void initGUI() {
		addBooleanProperty("L4-Variante", true);
	}
	
}
