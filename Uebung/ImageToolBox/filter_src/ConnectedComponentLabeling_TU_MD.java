import imageToolBox.AbstractFilter;

/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class ConnectedComponentLabeling_TU_MD extends AbstractFilter {

	int label = 0;
	int distanz = 1;
		
	@Override
	public void filter(double[][][] src, double[][][] dst) {
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				for (int k = 0; k < 3; k++) {
					dst[0][i][j] = label;
					dst[1][i][j] = src[1][i][j];
					dst[2][i][j] = src[2][i][j];
				}
			}
		}
		
		boolean labled = false;
		
		for (int j = distanz; j < height - distanz; j++) {
			for (int i = distanz; i < width - distanz; i++) {
				if (src[1][i][j] == 0) {
					labled = false;
					//Nachbarpixel auf label pr�fen
					for (int l = i-distanz; l <= i+distanz; l++) {
						for (int m = j-distanz; m <= j+distanz; m++) {
							if (dst[0][l][m] != 0) {
								//N�chste Pixel auch labeln falls notwendig, da sonst zu viele Labels vergeben werden bei schr�gen Kanten
								for (int u = i; u <= i+distanz; u++) {
									for (int v = j; v <= j+distanz; v++) {
										if (src[1][u][v] == 0) {
											dst[0][u][v] = dst[0][l][m];
										}
									}
								}
								labled = true;
								break;
							}
						}
						if(labled) break;
					}
					if(!labled){
						//N�chste Pixel auch labeln falls notwendig, da sonst zu viele Labels vergeben werden bei schr�gen Kanten
						for (int l = i; l <= i+distanz; l++) {
							for (int m = j; m <= j+distanz; m++) {
								if (src[1][l][m] == 0) {
									dst[0][l][m] = label;
								}
							}
						}
						label++;
					}
				}
			}
		}
		
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
		
				if (dst[0][i][j] == 0) {
					dst[0][i][j] = 255;
					dst[1][i][j] = 255;
					dst[2][i][j] = 255;
				} else if (dst[0][i][j] == 1) {
					dst[0][i][j] = 255;
					dst[1][i][j] = 0;
					dst[2][i][j] = 0;
				} else if (dst[0][i][j] == 2) {
					dst[0][i][j] = 0;
					dst[1][i][j] = 255;
					dst[2][i][j] = 0;
				} else if (dst[0][i][j] == 3) {
					dst[0][i][j] = 0;
					dst[1][i][j] = 0;
					dst[2][i][j] = 255;
				} else if (dst[0][i][j] == 4) {
					dst[0][i][j] = 255;
					dst[1][i][j] = 255;
					dst[2][i][j] = 0;
				} else if (dst[0][i][j] == 5) {
					dst[0][i][j] = 255;
					dst[1][i][j] = 0;
					dst[2][i][j] = 255;
				} else if (dst[0][i][j] == 6) {
					dst[0][i][j] = 0;
					dst[1][i][j] = 255;
					dst[2][i][j] = 255;
				} else if (dst[0][i][j] == 7) {
					dst[0][i][j] = 138;
					dst[1][i][j] = 43;
					dst[2][i][j] = 226;
				} else if (dst[0][i][j] == 8) {
					dst[0][i][j] = 255;
					dst[1][i][j] = 127;
					dst[2][i][j] = 0;
				} else if (dst[0][i][j] == 9) {
					dst[0][i][j] = 0;
					dst[1][i][j] = 0;
					dst[2][i][j] = 0;
					
				}
			}
		}
	}
}
