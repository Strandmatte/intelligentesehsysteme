/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class Mittelwertfilter5x5_TU_MD extends ConvolutionFilter_TU_MD {

	int dimension = 5;
	double teiler = (double) 1 / (dimension * dimension);

	@Override
	public double[][] getKernel() {
		double[][] kernel = new double[dimension][dimension];
		for (int i = 0; i < dimension; i++) {
			for (int j = 0; j < dimension; j++) {
				kernel[i][j] = teiler;
			}
		}

		return kernel;
	}
}
