import java.util.Arrays;

import imageToolBox.AbstractFilter;

/**
 * 
 * @author Tobias Umlauf, Markus Daugs
 * 
 */

public class Skelettierung_TU_MD extends AbstractFilter {

	// A=2, B=3, P=100; die 1 wurde durch 255 ersetzt
	double[][] kernel1 = { { 2, 2, 2 }, { 0, 100, 0 }, { 3, 3, 3 } };
	double[][] kernel2 = { { 2, 0, 3 }, { 2, 100, 3 }, { 2, 0, 3 } };
	double[][] kernel3 = { { 2, 2, 2 }, { 2, 100, 0 }, { 2, 0, 255 } };
	double[][] kernel4 = { { 2, 0, 255 }, { 2, 100, 0 }, { 2, 2, 2 } };
	double[][] kernel5 = { { 2, 2, 2 }, { 0, 100, 2 }, { 255, 0, 2 } };
	double[][] kernel6 = { { 255, 0, 2 }, { 0, 100, 2 }, { 2, 2, 2 } };

	@Override
	public void filter(double[][][] src, double[][][] dst) {

		boolean morePixelLeft = true;

		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				for (int k = 0; k < 3; k++) {
					dst[0][i][j] = src[0][i][j];
					dst[1][i][j] = src[1][i][j];
					dst[2][i][j] = src[2][i][j];
				}
			}
		}

		while (morePixelLeft == true) {
			morePixelLeft = false;

			for (int direction = 0; direction < 4; direction++) {
				for (int i = width-1; i >-1 ; i--) {
					for (int j =0 ; j <height ; j++) {
						innerloop: if (dst[0][i][j] == 255) {
							switch (direction) {
							case 0:
								if (dst[0][i][j-1] != 0)
									break innerloop;
							case 1:
								if (dst[0][i+1][j] != 0)
									break innerloop;
							case 2:
								if (dst[0][i][j+1] != 0)
									break innerloop;
							case 3:
								if (dst[0][i-1][j] != 0)
									break innerloop;								
							}
							if (findSkeleton(dst, i, j) == 255) {
								dst[0][i][j] = 255;
								dst[1][i][j] = 255;
								dst[2][i][j] = 255;
							} else {
								dst[0][i][j] = 0;
								morePixelLeft = true;
							}
						}
					}
				}
				for (int k = 0; k < height; k++) {
					for (int l = 0; l < width; l++) {
						if (dst[0][k][l] == 0) {
							dst[0][k][l] = 0;
							dst[1][k][l] = 0;
							dst[2][k][l] = 0;
						} else {
							dst[0][k][l] = 255;
							dst[1][k][l] = 255;
							dst[2][k][l] = 255;
						}
					}
				}
			}
		}
	}

	public double findSkeleton(double[][][] src, int i, int j) {

		double[][] submatrix = new double[3][3];
		double[][] kernel;
		double value = 0;

		for (int k = 0; k < 3; k++) {
			submatrix[k] = Arrays.copyOfRange(src[0][i - 1 + k], j - 1,
					j + 1 + 1);
		}

		for (int kernelNr = 0; kernelNr < 6; kernelNr++) {
			int countA = 0;
			int countB = 0;

			switch (kernelNr) {
			case 0:
				kernel = kernel1;
				break;
			case 1:
				kernel = kernel2;
				break;
			case 2:
				kernel = kernel3;
				break;
			case 3:
				kernel = kernel4;
				break;
			case 4:
				kernel = kernel5;
				break;
			case 5:
				kernel = kernel6;
				break;
			default:
				kernel = kernel1;
				break;
			}

			loop: for (int k = 0; k < 3; k++) {
				for (int l = 0; l < 3; l++) {					
					if (kernel[k][l] == 2 && submatrix[k][l] == 255) {
						countA++;
					}
					else if (kernel[k][l] == 3 && submatrix[k][l] == 255) {
						countB++;
					} 
					else if (kernel[k][l] == 0 && submatrix[k][l] == 255) {
						countA = 0;
						countB = 0;
						break loop;
					}
					else if (kernel[k][l] == 255 && submatrix[k][l] == 0) {
						countA = 0;
						countB = 0;
						break loop;
					}	
					else countB++;
				}
			}
			if ((countA > 0 && countB > 0)||(kernelNr>1 && countA>0)) return 255;
			else value = 0;
		}
		return value;
	}
}
